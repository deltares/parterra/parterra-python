# -*- coding: utf-8 -*-
"""
Handles all plots for ParTerra.

@author: haag (arjen.haag@deltares.nl)
"""


import os
import numpy as np
import pandas as pd
import geopandas as gpd

import matplotlib.pyplot as plt


def singlePlot(df, show_count=True, base_title='ParTerra features', path='', filename='ParTerra_SinglePlot'):
    if show_count:
        title = base_title + ' (' + str(df.count().geometry) + ' features)'
    else:
        title = base_title
    f, ax = plt.subplots(1, figsize=(8, 8))
    ax.set_aspect('equal')
    df.plot(ax=ax)
    ax.set_title(title)
    plt.savefig(os.path.join(path, filename))
    plt.close()


def subPlots(rows, cols, dfs, base_titles, show_count=True, set_bounds=True, buffer_pcnt=0, path='', filename='ParTerra_MultiPlot'):
    if (rows*cols >= len(dfs)):
        f, ax = plt.subplots(nrows=rows, ncols=cols, figsize=(7*cols, 6*rows))
        if set_bounds:
            bounds_all  = np.array(list(map(lambda x: x.total_bounds, dfs)))
            bounds_min  = bounds_all.min(0)
            bounds_max  = bounds_all.max(0)
            bounds      = [bounds_min[0], bounds_min[1], bounds_max[2], bounds_max[3]]
            range_x     = bounds_max[2] - bounds_min[0]
            range_y     = bounds_max[3] - bounds_min[1]
            bounds      = [bounds_min[0]-(range_x/100*buffer_pcnt), bounds_min[1]-(range_y/100*buffer_pcnt), \
                           bounds_max[2]+(range_x/100*buffer_pcnt), bounds_max[3]+(range_y/100*buffer_pcnt)]
        if (rows > 1) & (cols >1):
            for i in range(len(ax)):
                for j in range(len(ax[0])):
                    if (i*len(ax[0])+j) < len(dfs):
                        ax[i][j].set_aspect('equal')
                        dfs[i*len(ax[0])+j].plot(ax=ax[i][j])
                        if show_count:
                            ax[i][j].set_title(base_titles[i*len(ax[0])+j] + ' (' + str(dfs[i*len(ax[0])+j].count().geometry) + ' features)')
                        else:
                            ax[i][j].set_title(base_titles[i*len(ax[0])+j])
                        if set_bounds:
                            ax[i][j].set_xlim([bounds[0],bounds[2]])
                            ax[i][j].set_ylim([bounds[1],bounds[3]])
                    else:
                        f.delaxes(ax[i][j])
        else:
            for i in range(len(ax)):
                ax[i].set_aspect('equal')
                dfs[i].plot(ax=ax[i])
                if show_count:
                    ax[i].set_title(base_titles[i] + ' (' + str(dfs[i].count().geometry) + ' features)')
                else:
                    ax[i].set_title(base_titles[i])
                if set_bounds:
                    ax[i].set_xlim([bounds[0],bounds[2]])
                    ax[i].set_ylim([bounds[1],bounds[3]])
        f.tight_layout()
        plt.savefig(os.path.join(path, filename))
        plt.close()
    else:
        raise IndexError('Number of rows and columns does not match number of dataframes!')


def histograms(rows, cols, dfs, titles, path='', filename='ParTerra_Histogram'):
    if (rows*cols >= len(dfs)):
        f, ax = plt.subplots(nrows=rows, ncols=cols, figsize=(7*cols, 4*rows))
        if (rows > 1) & (cols >1):
            for i in range(len(ax)):
                for j in range(len(ax[0])):
                    if (i*len(ax[0])+j) < len(dfs):
                        dfs[i*len(ax[0])+j].count().plot(ax=ax[i][j], kind='bar')
                        ax[i][j].set_title('OSM ' + titles[i*len(ax[0])+j] + ' histogram')
                    else:
                        f.delaxes(ax[i][j])
        else:
            for i in range(len(ax)):
                dfs[i].count().plot(ax=ax[i], kind='bar')
                ax[i].set_title('OSM ' + titles[i] + ' histogram')
        f.tight_layout()
        plt.savefig(os.path.join(path, filename))
        plt.close()
    else:
        raise IndexError('Number of rows and columns does not match number of dataframes!')


def histogramsForColumn(rows, cols, dfs, titles, dropNaNs=True, path='', filename='ParTerra_HistogramColumn'):
    if (rows*cols >= len(dfs)):
        f, ax = plt.subplots(nrows=rows, ncols=cols, figsize=(7*cols, 4*rows))
        if (rows > 1) & (cols >1):
            for i in range(len(ax)):
                for j in range(len(ax[0])):
                    if (i*len(ax[0])+j) < len(dfs):
                        dfs[i*len(ax[0])+j].value_counts(dropna=dropNaNs).plot(ax=ax[i][j], kind='bar')
                        ax[i][j].set_title('OSM ' + titles[i*len(ax[0])+j] + ' histogram')
                    else:
                        f.delaxes(ax[i][j])
        elif (rows > 1) | (cols >1):
            for i in range(len(ax)):
                dfs[i].value_counts(dropna=dropNaNs).plot(ax=ax[i], kind='bar')
                ax[i].set_title('OSM ' + titles[i] + ' histogram')
        else:
            dfs[0].value_counts(dropna=dropNaNs).plot(ax=ax, kind='bar')
            ax.set_title('OSM ' + titles[0] + ' histogram')
        f.tight_layout()
        plt.savefig(os.path.join(path, filename))
        plt.close()
    else:
        raise IndexError('Number of rows and columns does not match number of dataframes!')
