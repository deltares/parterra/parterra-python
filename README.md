# ParTerra-Python

The Participatory Terrain model deploys an algorithm to fuse together data from OpenStreetMap (OSM) and any base elevation dataset to create a high-resolution digital terrain model for any area in the world.

ParTerra was originally coded in Google Earth Engine (GEE) JavaScript/Python (see https://gitlab.com/deltares/parterra/osm-terrain-app).
However, for various reasons it was decided to convert the algorithm to non-GEE Python, of which this is the latest version.

**This project is still work-in-progress.**

# Output
ParTerra outputs a Digital Surface Model (DSM) ["as the crow flies"] and Digital Terrain Model (DTM) ["as the water flows"].

![ParTerra input and output](docs/ParTerra_Amsterdam_2.png "ParTerra input and output")

An online application for viewing and analysing some preliminary results over several regions can be found at https://arjenhaag.users.earthengine.app/view/parterra

Optionally, ParTerra can also produce a Manning's roughness map, specifically designed for use in hydrodynamic / hydraulic models.

# Steps
The steps needed to obtain ParTerra elevation models are listed below. Because of the legacy and work-in-progress status of this project, there are currently two options for obtaining a base DEM (but we plan to move to a single dedicated solution later).
- download OSM data (e.g. from https://export.hotosm.org/ using the `HOT_OSM_export.yml`)
- obtain a base DEM
    - option 1: use [GEE DEM preprocessing script](https://gitlab.com/deltares/parterra/parterra-python/-/blob/master/legacy/ParTerra_prep_DEM.js)
        - add bounding box in script (can easily be copied from GeoJSON that comes with HOT OSM export) 
        - adjust parameters in script (e.g. EPSG code, DEM resolution)
        - run script, exporting DEM to your Google Drive
        - once done, download DEM from your Google Drive
    - option 2: download from anywhere and do preprocessing within ParTerra-Python
        - set desired resolution in settings file (default = 1 meter)
- adjust settings (in `settings.ini`)
- optionally, run the `gis_template.py` script to obtain a new QGIS project to inspect the input data
- adjust OSM filters (in `Filters.xlsm`, which will automatically export the required CSV files)
- run the `parterra.py` script

# Documentation
A detailed user manual is included in the [docs](https://gitlab.com/deltares/parterra/parterra-python/-/tree/master/docs) directory.

# Publications
- Haag, A., et al. (2019). High-resolution elevation data for vulnerability assessment of roads in a changing climate. Proceedings of the PIARC World Road Congress 2019. Available at https://proceedings-abudhabi2019.piarc.org/en/documents/individual-papers/s-1997.htm
- Gebremedhin, E.T., et al. (2020). Crowdsourcing and interactive modelling for urban flood management. Journal of Flood Risk Management, 13(2). doi: https://doi.org/10.1111/jfr3.12602

# Contribute
Please see the [Contributing Guidelines](https://gitlab.com/deltares/parterra/parterra-python/-/blob/master/CONTRIBUTING.md) for details. As of now, only project members can contribute. Please contact one of the owners (e.g. arjen.haag@deltares.nl) for related requests. We plan to move towards completely open contribution, so stay tuned for more information.

# License
ParTerra is available under the open source [MIT License](https://gitlab.com/deltares/parterra/parterra-python/-/blob/master/LICENSE).

# Citation
You are kindly asked to cite this work as follows, pending a more official DOI-based citation:

Haag, A. [Deltares] (2020) ParTerra-Python (Version 1.1) [Source code]. https://gitlab.com/deltares/parterra/parterra-python.
