# Contributing

Thanks for considering contributing to ParTerra!

As of now, only project members can contribute. Please contact one of the owners (e.g. arjen.haag@deltares.nl) for related requests.

We plan to move towards completely open contribution, so stay tuned for more information.