# -*- coding: utf-8 -*-
"""
@author: haag (arjen.haag@deltares.nl)

Updates QGIS template for new ParTerra project.
"""


import os
import sys
import configparser as cp

import rasterio

import zipfile
from xml.etree import ElementTree as ET

import utils


# get settings file
if len(sys.argv) > 1:
    settings_file = sys.argv[1]
else:
    settings_file = 'settings.ini'

# read settings
config = cp.ConfigParser()
try:
    config.read(settings_file)
except:
    sys.exit("ERROR: Not possible to open settings file.")

if config.has_section("STRUCTURE"):
    path_input        = utils.check_key(config["STRUCTURE"],"path_input", os.getcwd())
    dir_osm           = utils.check_key(config["STRUCTURE"],"dir_osm", 'data')
    lines_file        = utils.check_key(config["STRUCTURE"],"lines_file", 'osm_lines_lines.shp')
    polys_file        = utils.check_key(config["STRUCTURE"],"polys_file", 'osm_polys_polygons.shp')
    bounds_file       = utils.check_key(config["STRUCTURE"],"bounds_file", 'clipping_boundary.geojson')
    dir_dem           = utils.check_key(config["STRUCTURE"],"dir_dem", '')
    dem_file          = utils.check_key(config["STRUCTURE"],"dem_file", 'DEM.tif')
    path_output       = utils.check_key(config["STRUCTURE"],"path_output", os.getcwd())
    dir_shapes_out    = utils.check_key(config["STRUCTURE"],"dir_shapes_out", 'output_shapefiles')
    dir_rasters_out   = utils.check_key(config["STRUCTURE"],"dir_rasters_out", 'output_rasters')
    path_gis_template = utils.check_key(config["STRUCTURE"],"path_gis_templ", "qgis\\QGIS_template.qgz")
    gis_project_file  = utils.check_key(config["STRUCTURE"],"gis_project_file", 'QGIS_ParTerra')
if config.has_section("PARS"):
    epsg_code         = utils.check_key(config["PARS"],"epsg_code")

# construct and check paths
path_OSM = os.path.join(path_input, dir_osm)
path_DEM = os.path.join(path_input, dir_dem)
if not os.path.isabs(path_gis_template):
    path_gis_template = os.path.abspath(path_gis_template)
if not os.path.exists(path_gis_template):
    sys.exit("ERROR: specified path to GIS template does not exist!")
if not os.path.exists(path_OSM):
    sys.exit("ERROR: specified path to OSM data does not exist!")
if not os.path.exists(os.path.join(path_DEM, dem_file)):
    sys.exit("ERROR: specified path to DEM file does not exist!")

# read base DEM
with rasterio.open(os.path.join(path_DEM, dem_file)) as src:
    map_meta = src.meta
# check projection (EPSG code)
epsg_code = utils.checkEPSG(epsg_code, map_meta['crs'].to_epsg())

# set relevant properties for updating projection info
proj4_txt = "+proj=utm +zone=" + str(epsg_code)[-2:]
srsid  = "31" + str(epsg_code-16)[-2:]
srid   = str(epsg_code)
authid = "EPSG:" + str(epsg_code)
descr  = "WGS 84 / UTM zone " + str(epsg_code)[-2:]
if str(epsg_code)[-3] == '6':
    descr += "N"
elif str(epsg_code)[-3] == '7':
    descr += "S"
    proj4_txt += " +south"
else:
    sys.exit("ERROR: could not find North or South UTM zone from EPSG code!")
proj4_txt += " +datum=WGS84 +units=m +no_defs"

# function for updating spatial reference system
def updateSRS(srs):
    srs.find('proj4').text = proj4_txt
    srs.find('srsid').text = srsid
    srs.find('srid').text = srid
    srs.find('authid').text = authid
    srs.find('description').text = descr

# set relevant properties for updating map layers
ids_mapping = {}
layers_mapping = {
    'boundary': os.path.join(path_OSM, bounds_file),
    'polygons': os.path.join(path_OSM, polys_file),
    'lines': os.path.join(path_OSM, lines_file),
    'DEM': os.path.join(path_DEM, dem_file)
}

# function for updating map layers
def updateLayer(layer):
    id = layer.attrib['id']
    for key in layers_mapping.keys():
        if key in id:
            layer.attrib['source'] = layers_mapping[key]
            ids_mapping[id] = layers_mapping[key]
            if key == 'DEM':
                layer.attrib['name'] = dem_file

# open QGIS project file
with zipfile.ZipFile(path_gis_template, 'r') as zf:
    with zf.open(zf.namelist()[0], 'r') as f:
        # parse XML code
        tree = ET.parse(f)
        project = tree.getroot()
        # update project crs
        updateSRS(project.find('projectCrs').find('spatialrefsys'))
        # update layer tree
        layers_top = project.find('layer-tree-group')
        for layer_1 in layers_top:
            if 'layer' in layer_1.tag:
                if 'group' in layer_1.tag:
                    for layer_2 in layer_1:
                        if 'layer' in layer_2.tag:
                            if 'group' in layer_2.tag:
                                for layer_3 in layer_2:
                                    if 'layer' in layer_3.tag:
                                        if 'group' in layer_3.tag:
                                            print("WARNING: Found layer group with level 3 or deeper! Updating of this deep nested layers is not yet supported!")
                                        else:
                                            updateLayer(layer_3)
                            else:
                                updateLayer(layer_2)
                else:
                    updateLayer(layer_1)
        # update project layers
        projectlayers = project.find('projectlayers')
        for layer in projectlayers:
            if layer.find('id').text in ids_mapping.keys():
                layer.find('datasource').text = ids_mapping[layer.find('id').text]
                if 'DEM' in layer.find('id').text:
                    layer.find('layername').text = dem_file
                    updateSRS(layer.find('srs').find('spatialrefsys'))
        # write to file
        tree.write(os.path.join(path_output, gis_project_file + '.qgs'))
        # zip file
        with zipfile.ZipFile(os.path.join(path_output, gis_project_file + '.qgz'), 'w', zipfile.ZIP_DEFLATED) as qzf:
            qzf.write(os.path.join(path_output, gis_project_file + '.qgs'), gis_project_file + '.qgs')
        os.remove(os.path.join(path_output, gis_project_file + '.qgs'))
