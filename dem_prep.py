# -*- coding: utf-8 -*-
"""
DEM preprocessing functions for ParTerra.

@author: haag (arjen.haag@deltares.nl)
"""


import os
import numpy as np
import scipy
import scipy.ndimage

import rasterio
from rasterio.enums import Resampling

import utils


def resampleDEM(src, factor):
    # resampling (https://rasterio.readthedocs.io/en/latest/topics/resampling.html)
    resampled_DEM = src.read(out_shape=(src.count, int(src.height * factor), int(src.width * factor)), resampling=Resampling.bilinear)
    return resampled_DEM

def peronaMalikFilter(data, iter=5, K=5, delta=0.2, method=2):
    
    dxW = np.array([[0, 0, 0], [1, -1, 0], [0, 0, 0]])
    dxE = np.array([[0, 0, 0], [0, -1, 1], [0, 0, 0]])
    dyN = np.array([[0, 1, 0], [0, -1, 0], [0, 0, 0]])
    dyS = np.array([[0, 0, 0], [0, -1, 0], [0, 1, 0]])
    
    pm_lambda = np.ones(data.shape)*delta
    
    k1 = np.ones(data.shape)*(-1.0/K)
    k2 = np.ones(data.shape)*K*K
    
    for i in range(iter):
        
        dI_W = scipy.ndimage.filters.convolve(data, dxW)
        dI_E = scipy.ndimage.filters.convolve(data, dxE)
        dI_N = scipy.ndimage.filters.convolve(data, dyN)
        dI_S = scipy.ndimage.filters.convolve(data, dyS)
        
        if method == 1:
            
            cW = np.exp(dI_W * dI_W * k1)
            cE = np.exp(dI_E * dI_E * k1)
            cN = np.exp(dI_N * dI_N * k1)
            cS = np.exp(dI_S * dI_S * k1)
            
        elif method == 2:
            
            cW = np.ones(data.shape) / (np.ones(data.shape) + (dI_W * dI_W / k2))
            cE = np.ones(data.shape) / (np.ones(data.shape) + (dI_E * dI_E / k2))
            cN = np.ones(data.shape) / (np.ones(data.shape) + (dI_N * dI_N / k2))
            cS = np.ones(data.shape) / (np.ones(data.shape) + (dI_S * dI_S / k2))
            
        data = data + (pm_lambda * (cN * dI_N + cS * dI_S + cE * dI_E + cW * dI_W))
    
    return data

def prepDEM(src, base_DEM, path_out, dir_out, target_res=1, gaus_sigma=15, auto_t=True, pm_iter=5, pm_k=5, pm_delta=0.2, pm_method=2, save_temp=False, file_temp_DEM='temp_DEM', logger=None):
    # save base DEM as float
    map_meta = src.meta
    base_res = abs(map_meta['transform'][0])
    factor   = base_res / target_res
    map_meta.update(compress='lzw')
    map_meta['dtype'] = 'float64'
    utils.writeRasterToFile(base_DEM*1.0, os.path.join(path_out, dir_out, file_temp_DEM + '_float.tif'), map_meta, digits=4)
    src.close()
    # read in resampled data (now in float format)
    src = rasterio.open(os.path.join(path_out, dir_out, file_temp_DEM + '_float.tif'))
    # resample to target resolution
    if logger != None:
        logger.info('Resampling base DEM using factor ' + str(factor))
    resampled_DEM = resampleDEM(src, factor)
    transform = src.transform * src.transform.scale((src.width / resampled_DEM.shape[-1]), (src.height / resampled_DEM.shape[-2]))
    src.close()
    if not save_temp:
        os.remove(os.path.join(path_out, dir_out, file_temp_DEM + '_float.tif'))
    else:
        if logger != None:
            logger.info('Saving intermediate files of DEM resampling and smoothing to ' + os.path.join(path_out, dir_out))
    # update map metadata
    map_meta['transform'] = transform
    map_meta['width']     = resampled_DEM.shape[-1]
    map_meta['height']    = resampled_DEM.shape[-2]
    if save_temp:
        utils.writeRasterToFile(resampled_DEM[0], os.path.join(path_out, dir_out, file_temp_DEM + '_to' + str(int(target_res)) + 'm.tif'), map_meta)
    # apply Gaussian filter (https://docs.scipy.org/doc/scipy/reference/generated/scipy.ndimage.gaussian_filter.html)
    gaus_radius = factor
    gaus_sigma  = gaus_sigma / target_res
    if auto_t:
        t = (((gaus_radius - 1)/2)-0.5)/gaus_sigma # https://stackoverflow.com/questions/25216382/gaussian-filter-in-scipy
    else:
        t = 4.0 # default
    if logger != None:
        logger.info('Applying Gaussian filter, with sigma ' + str(gaus_sigma) + ', truncated at ' + str(t))
    DEM_smoothed_1 = scipy.ndimage.gaussian_filter(resampled_DEM[0], sigma=gaus_sigma, output=np.float64, mode='nearest', truncate=t)
    if save_temp:
        utils.writeRasterToFile(DEM_smoothed_1, os.path.join(path_out, dir_out, file_temp_DEM + '_to' + str(int(target_res)) + 'm_prep1_gaus_radius' + str(int(gaus_radius)) + '_sigma' + str(int(gaus_sigma)) + '.tif'), map_meta)
    # apply Perona Malik filter
    if logger != None:
        logger.info('Applying Perona Malik filter, with kappa ' + str(pm_k) + ' and ' + str(pm_iter) + ' iterations')
    DEM_smoothed_2 = peronaMalikFilter(DEM_smoothed_1, iter=pm_iter, K=pm_k, delta=pm_delta, method=pm_method)
    if save_temp:
        utils.writeRasterToFile(DEM_smoothed_2, os.path.join(path_out, dir_out, file_temp_DEM + '_to' + str(int(target_res)) + 'm_prep2_PM_iter ' + str(pm_iter) + '_k' + str(pm_k) + '.tif'), map_meta)
    return DEM_smoothed_2, map_meta
