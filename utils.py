# -*- coding: utf-8 -*-
"""
Includes various functions/utilities for ParTerra.

@author: haag (arjen.haag@deltares.nl)
"""

import os
import sys
import subprocess

import shapely
from shapely.geometry import box

import numpy as np
import pandas as pd
import geopandas as gpd

import rasterio
from rasterio import features

import create_figures


# check entry in settings ini file
def check_key(dictionary, key, default_value=None):
    """
    Returns the value assigned to the 'key' in the ini file.
    
    Parameters:
        dictionary : [dict] 
        key : [string|int] 
        default_value : [string] 
    
    Output: Value assigned to the 'key' into the 'dictionary' (or default value if not found)
    """
    if key in dictionary.keys():
        return dictionary[key]
    else:
        return default_value

# derive projection (EPSG code) from region
def getUTMzone(bounds):
    temp_centroid = bounds.geometry.centroid[0]
    temp_zone_x   = np.floor( (temp_centroid.x + 180) / 6 ) + 1
    if temp_centroid.y >= 0:
        #temp_zone_y = 'north'
        #temp_epsg   = 'EPSG:326' + temp_zone_x
        temp_epsg   = 32600 + temp_zone_x
        #zoneinfo    = temp_zone_x + ' N'
    else:
        #temp_zone_y = 'south'
        #temp_epsg   = 'EPSG:327' + temp_zone_x
        temp_epsg   = 32700 + temp_zone_x
        #zoneinfo    = temp_zone_x + ' S'
    #temp_zone_string = '+proj=utm +zone=' + temp_zone_x + ' +' + temp_zone_y + ' +datum=WGS84 +units=m +no_defs'
    #return [temp_epsg, temp_zone_string, zoneinfo]
    return int(temp_epsg)

# print UTM zone to console
def showUTMzone():
    # get path to bounds file
    if len(sys.argv) > 1:
        path_bounds = sys.argv[1]
    else:
        path_bounds = 'clipping_boundary.geojson'
    if not os.path.exists(path_bounds):
        sys.exit("ERROR: specified path to bounds file does not exist!")
    # read bounds file
    df_bounds = gpd.read_file(path_bounds)
    # derive UTM zone
    temp_epsg = getUTMzone(df_bounds)
    print('Automatically derived EPSG code: ' + str(temp_epsg))

# check/update projection (EPSG code)
def checkEPSG(bounds, epsg_code, epsg_dem, logger=None):
    epsg_auto = getUTMzone(bounds)
    if epsg_code != None:
        epsg_code = int(epsg_code)
        if epsg_code != epsg_dem:
            if logger != None:
                logger.warning('EPSG code specified in settings (' + str(epsg_code) + ') does not match with base DEM (' + str(epsg_dem) +')! This can lead to errors!')
        if epsg_code != epsg_auto:
            if logger != None:
                logger.warning('EPSG code specified in settings (' + str(epsg_code) + ') does not match with automatically derived version (' + str(epsg_auto) +')! Check if specified version is correct!')
    else:
        if logger != None:
            logger.info('EPSG code not specified in settings, using base DEM crs')
        if epsg_dem != epsg_auto:
            if logger != None:
                logger.warning('EPSG code from base DEM (' + str(epsg_dem) + ') does not match with automatically derived version (' + str(epsg_auto) +')! Check if base DEM has correct projection!')
        epsg_code = epsg_dem
    return epsg_code

# convert string-list from settings to regular list
def convertStringToList(list):
    if list != None:
        list = list.split(',')
    else:
        list = []
    return list

# correct or remove incorrect entries
def applyCorrections(df, columns, name, replace_user=None, replace_default={',':'.', ' ':'', 'o':'0', 'O':'0', 'meters':'', 'METERS':'', 'Meters':'', 'meter':'', 'METER':'', 'Meter':'', 'm':'', 'M':'', 's':'', 'feet':'', 'ft':'', 'C':'', "'":""}):
    #logger.info('for ' + name + ': ' + str(columns))
    if replace_user != None:
        for column in columns:
            df.loc[:,column].replace(replace_user, inplace=True, regex=True)
    for column in columns:
        df.loc[:,column].replace(replace_default, inplace=True, regex=True)

# convert columns to numeric type
def convertToNumeric(df, columns, name):
    #logger.info('for ' + name + ': ' + str(columns))
    for column in columns:
        df.loc[:,column] = pd.to_numeric(df[column])
        if column == 'width':
            df.loc[:,column].replace(0, np.nan, inplace=True)

# create new (empty) columns
def addNewColumns(df, filter, df_name):
    for key in filter.columns:
        if key not in df.columns:
            if key not in ['name', 'values', 'area']:
                #logger.info(df_name + ': ' + key)
                df.insert(len(df.columns), key, list(np.full(len(df.index), None)))
    df.insert(len(df.columns), 'DTM_height', list(np.full(len(df.index), None)))
    df.insert(len(df.columns), 'DSM_height', list(np.full(len(df.index), None)))

# fill columns with values
def assignColumnValues(df, filter, df_name, columns):
    if not df.empty:
        # loop over all specific filters
        for i in range(len(filter)-1):
            #logger.info(df_name + 's: ' + str(filter.iloc[i]['values']))
            for column in columns:
                df.loc[df[df_name].isin(filter.iloc[i]['values'].split(',')) & df[column].isnull(), column] = filter.iloc[i][column]
        # all those not included in specific filters (i.e. 'other')
        #logger.info(df_name + 's: others')
        for column in columns:
            df.loc[df[column].isnull(), column] = filter.iloc[-1][column]

# cap values at specified maximum
def capMax(df, columns, name):
    #logger.info('for ' + name + ': ' + str(columns))
    for column in columns:
        df.loc[:,column].clip(upper=df['max_'+column], inplace=True)

# set embankments for infrastructure features
def setEmbankments(df_infra, df_embank, column_infra, column_embank='embankment', column_embank_height='embank_height', exclude_embankment_types=['levee','dike','dyke','embankment','reinforced_slope'], logger=None):
    if not df_embank.empty:
        if logger != None:
            logger.info('Processing embankment features for ' + column_infra + 's')
        # set all non-specifc embankment types to 'embankment'
        df_infra.loc[(df_infra[column_embank].notnull()) & (df_infra[column_embank]!='no') & (~df_infra[column_embank].isin(exclude_embankment_types)), column_embank] = 'embankment'
        # set all embankment heights to zero for features without embankment
        df_infra.loc[(df_infra[column_embank].isnull()) | (df_infra[column_embank]=='no'), column_embank_height] = 0
        # set widths and heights
        for embankment in df_embank[column_embank].unique():
            if logger != None:
                logger.info("Number of features with embankment type '" + embankment + "': " + str(len(df_infra.loc[df_infra[column_embank]==embankment])))
            # set embankment width to maximum of infrastructure and embankment width
            df_embank.loc[df_embank[column_infra].notnull(), 'width'] = np.maximum(df_embank.loc[df_embank[column_infra].notnull(), 'width'].astype(float), df_infra.loc[df_infra[column_embank]==embankment, 'width'].astype(float))
            # set infrastructre height to maximum of infrastructure and embankment height, when overrule_infra is set to 1
            osm_ids = df_embank.loc[(df_embank[column_embank]==embankment) & (df_embank['overrule_infra']==1), 'osm_id'].values.tolist()
            df_infra.loc[df_infra['osm_id'].isin(osm_ids), column_embank_height] = np.maximum(df_infra.loc[df_infra['osm_id'].isin(osm_ids), column_embank_height].astype(float), df_embank.loc[(df_embank[column_infra].notnull()) & (df_embank['osm_id'].isin(osm_ids)), 'height'].astype(float))

# split MultiLineString (used for sidewalks as those lines need to be extended, see extendLine)
def splitMultiGeoms(gdf):
    gdf_single  = gdf.loc[gdf.geometry.type=='LineString']
    gdf_multi   = gdf.loc[gdf.geometry.type=='MultiLineString']
    gdf_multi_2 = gdf_multi.copy()
    for index, row in gdf_multi.iterrows():
        for i in range(len(row.geometry)):
            if i == 0:
                gdf_multi_2.loc[gdf_multi_2['osm_id']==row['osm_id'], 'geometry'] = row.geometry[0]
                gdf_multi_2.loc[gdf_multi_2['osm_id']==row['osm_id'], 'osm_id']   = row['osm_id'] + '_0'
            else:
                gdf_multi_2.loc[len(gdf_multi_2)] = row
                gdf_multi_2.loc[gdf_multi_2['osm_id']==row['osm_id'], 'geometry'] = row.geometry[i]
                gdf_multi_2.loc[gdf_multi_2['osm_id']==row['osm_id'], 'osm_id']   = row['osm_id'] + '_' + str(i)
    new_gdf = gdf_single.append(gdf_multi_2, ignore_index=True, sort=False)
    return new_gdf

# extrapolate to a new point
def extrapolatePoint(point_1, point_2, length):
    # get individual points
    x1    = point_1[0]
    y1    = point_1[1]
    x2    = point_2[0]
    y2    = point_2[1]
    # calculate lengths and direction
    len_x = (x1-x2)**2
    len_y = (y1-y2)**2
    len   = (len_x+len_y)**0.5
    sin   = (x2-x1)/len
    cos   = y2-y1/len
    ext   = (len+float(length))/len
    # obtain extrapolated point
    x3    = x2+((x1-x2)*ext)
    y3    = y2+((y1-y2)*ext)
    return (x3,y3)

# extend line based on width
def extendLineStart(line, width):
    # get coordinates of last two points
    coords         = list(line.coords)
    coords_start_2 = coords[1]
    coords_start_1 = coords[0]
    # create new, extrapolated point from these
    coords_start_0 = extrapolatePoint(coords_start_1, coords_start_2, width)
    # add new points to existing coordinates and create new line feature
    coords_ext     = coords.copy()
    coords_ext.insert(0, coords_start_0)
    return shapely.geometry.LineString(coords_ext)
def extendLineEnd(line, width):
    # get coordinates of last two points
    coords         = list(line.coords)
    coords_end_2   = coords[-2]
    coords_end_1   = coords[-1]
    # create new, extrapolated point from these
    coords_end_0   = extrapolatePoint(coords_end_1, coords_end_2, width)
    # add new points to existing coordinates and create new line feature
    coords_ext     = coords.copy()
    coords_ext.append(coords_end_0)
    return shapely.geometry.LineString(coords_ext)
def extendLine(line, width):
    # get coordinates of last two points
    coords         = list(line.coords)
    coords_start_2 = coords[1]
    coords_start_1 = coords[0]
    # create new, extrapolated point from these
    coords_start_0 = extrapolatePoint(coords_start_1, coords_start_2, width)
    coords_end_2   = coords[-2]
    coords_end_1   = coords[-1]
    coords_end_0   = extrapolatePoint(coords_end_1, coords_end_2, width)
    # add new points to existing coordinates and create new line feature
    coords_ext     = coords.copy()
    coords_ext.insert(0, coords_start_0)
    coords_ext.append(coords_end_0)
    return shapely.geometry.LineString(coords_ext)

# buffer line segments based on width
def bufferWidth(row):
    return row.geometry.buffer(float(row.width))
def bufferRoad(row):
    return row.geometry.buffer(float(row.width)*float(row.drive_frac))
def bufferSidewalk(row):
    road      = row.geometry.buffer(float(row.width)) # full road
    driveway  = extendLine(row.geometry, row.width).buffer(float(row.width)*float(row.drive_frac)) # driveway, extended on both sides to prevent sidewalk from enclosing road
    return road.difference(driveway) # sidewalk = full road - (extended) driveway
def bufferAll(df, type, do_figures=False, do_sidewalks=True, path_figures=os.getcwd()):
    #logger.info(type + '...')
    if type == 'roads':
        if do_sidewalks:
            df_buffered = df.apply(bufferRoad, axis=1)
        else:
            df_buffered = df.apply(bufferWidth, axis=1)
    elif type == 'sidewalks':
        df_buffered = df.apply(bufferSidewalk, axis=1)
    else:
        df_buffered = df.apply(bufferWidth, axis=1)
    if do_figures:
        create_figures.singlePlot(df, show_count=False, base_title='ParTerra ' + type + ' (lines)', path=path_figures, filename='ParTerra_' + type)
    df.loc[:,'geometry'] = df_buffered
    if do_figures:
        create_figures.singlePlot(df, show_count=False, base_title='ParTerra ' + type + ' (polygons)', path=path_figures, filename='ParTerra_' + type + '_buffered')

# get sea polygon(s) from coastlines
def getSeaPolygon(coastlines, sea_point, bounds, dataframe_columns, logger=None):
    # get bounding box
    #bbox_minx = min(coastlines.geometry.bounds.minx)
    #bbox_miny = min(coastlines.geometry.bounds.miny)
    #bbox_maxx = max(coastlines.geometry.bounds.maxx)
    #bbox_maxy = max(coastlines.geometry.bounds.maxy)
    bbox_minx = min(bounds.geometry.bounds.minx)
    bbox_miny = min(bounds.geometry.bounds.miny)
    bbox_maxx = max(bounds.geometry.bounds.maxx)
    bbox_maxy = max(bounds.geometry.bounds.maxy)
    bbox = shapely.geometry.box(bbox_minx, bbox_miny, bbox_maxx, bbox_maxy)
    # merge coastlines into larger, continious lines
    coastlines_multi  = shapely.geometry.MultiLineString(list(coastlines.geometry.values))
    coastlines_merged = shapely.ops.linemerge(coastlines_multi)
    # split on points where coastlines and bounding box exterior touch
    if bbox.exterior.intersects(coastlines_merged):
        coastlines_merged = bbox.exterior.intersection(coastlines_merged)
    # check if coastlines intersect bounding box on both ends, if not extend line
    if coastlines_merged.type == 'LineString':
        coastline_0 = shapely.geometry.LineString(coastlines_merged.coords[0:-1])
        coastline_1 = shapely.geometry.LineString(coastlines_merged.coords[1:len(coastlines_merged.coords)])
        if not bbox.exterior.intersects(coastline_0):
            logger.warning('Part of coastlines does not intersect with bounding box! Extending coastline...')
            distance  = shapely.geometry.Point(coastlines_merged.coords[0]).distance(bbox.exterior)
            coastlines_merged = extendLineStart(coastlines_merged, distance*10)
        if not bbox.exterior.intersects(coastline_1):
            logger.warning('Part of coastlines does not intersect with bounding box! Extending coastline...')
            distance  = shapely.geometry.Point(coastlines_merged.coords[-1]).distance(bbox.exterior)
            coastlines_merged = extendLineEnd(coastlines_merged, distance*10)
    else:
        coastlines_list = []
        for coastline in coastlines_merged:
            coastline_0 = shapely.geometry.LineString(coastline.coords[0:-1])
            coastline_1 = shapely.geometry.LineString(coastline.coords[1:len(coastline.coords)])
            if not bbox.exterior.intersects(coastline_0):
                logger.warning('Part of coastlines does not intersect with bounding box! Extending coastline...')
                distance  = shapely.geometry.Point(coastline.coords[0]).distance(bbox.exterior)
                coastline = extendLineStart(coastline, distance*10)
            if not bbox.exterior.intersects(coastline_1):
                logger.warning('Part of coastlines does not intersect with bounding box! Extending coastline...')
                distance  = shapely.geometry.Point(coastline.coords[-1]).distance(bbox.exterior)
                coastline = extendLineEnd(coastline, distance*10)
            coastlines_list.append(coastline)
        coastlines_merged = shapely.geometry.MultiLineString(coastlines_list)
    # cut bounding box by coastlines, ending up with sea polygon(s)
    sea_point = shapely.geometry.Point(sea_point)
    sea_poly  = bbox
    if coastlines_merged.type == 'LineString':
        # split bounding box on coastline
        sea_poly_split = shapely.ops.split(sea_poly, coastlines_merged)
        # find part that contains sea point
        for part in sea_poly_split:
            if part.contains(sea_point):
                sea_poly = part
    else:
        for coastline in coastlines_merged:
            # split bounding box on coastline
            sea_poly_split = shapely.ops.split(sea_poly, coastline)
            # find part that contains sea point
            for part in sea_poly_split:
                if part.contains(sea_point):
                    sea_poly = part
    # construct GeoDataFrame for merging with water polygons
    sea_poly_df  = pd.DataFrame({}, index=[0])
    for column in dataframe_columns:
        sea_poly_df.insert(len(sea_poly_df.columns), column, None)
    sea_poly_df.loc[:,'natural'] = 'sea'
    sea_poly_df.loc[:,'water'] = 'sea'
    sea_poly_df.loc[:,'z_index'] = 0
    sea_poly_df.loc[:,'geometry'] = sea_poly
    sea_poly_gdf = gpd.GeoDataFrame(sea_poly_df, geometry='geometry')
    return sea_poly_gdf

# convert LinearRing to Polygon
def checkLinearRing(row):
    return row.geometry.is_ring
def convertToPolygon(row):
    return shapely.geometry.Polygon(row.geometry)
def convertRingsToPolys(features):
    features['is_ring'] = features.apply(checkLinearRing, axis=1)
    features_for_conversion = features.loc[features['is_ring']].copy()
    if not features_for_conversion.empty:
        features_for_conversion.geometry = features_for_conversion.apply(convertToPolygon, axis=1)
    return features_for_conversion

# remove irrelevant columns
def removeColumns(df, columns):
    # compare to-be-removed columns against columns present in data
    columns = list(set(columns) & set(df.columns))
    # remove columns
    df.drop(columns, axis=1, inplace=True)

# set up temp dir
def setupTempDir(path, temp_dir='temp'):
    if not os.path.exists(os.path.join(path, temp_dir)):
        #logger.info('Creating temporary output dir for DEM straightening, ' + os.path.join(path_output, dir_rasters_out, temp_dir))
        os.mkdir(os.path.join(path, temp_dir))
    else:
        #logger.info('Deleting all files in temporary output dir for DEM straightening, ' + os.path.join(path_output, dir_rasters_out, temp_dir))
        for to_delete_file in os.listdir(os.path.join(path, temp_dir)):
            os.remove(os.path.join(path, temp_dir, to_delete_file))
    return os.path.join(path, temp_dir)

# dissolve geometries within a GeoDataFrame
def dissolveFeatures(gdf, dissolve_column='width', agg_func='first'):
    #https://geopandas.org/aggregation_with_dissolve.html
    if dissolve_column == 'dissolve_all':
        # dissolve all features, returning a single feature
        gdf.insert(len(gdf.columns), 'dissolve', list(np.full(len(gdf.index), 1)))
        gdf_dissolved = gdf.dissolve(by='dissolve', aggfunc=agg_func)
    else:
        # dissolve features based on attribute, resulting in as many features as unique attribute values
        gdf_dissolved = gdf.dissolve(by=dissolve_column, aggfunc=agg_func)
        gdf_dissolved[dissolve_column] = gdf_dissolved.index
        gdf_dissolved.index = np.arange(len(gdf_dissolved))
    return gdf_dissolved

# calculate DTM/DSM heights
def setEmptyColumn(df, column=None):
    if column != None:
        value = df[column].astype(np.float)
    else:
        value = 0.0
    return value
def calcHeights(df, columns, column_offset=None, column_layer=None, column_layer_height=None, column_subtract=None, column_embank=None, factor=1.0):
    # set relevant column values
    embankment   = setEmptyColumn(df, column_embank)
    offset       = setEmptyColumn(df, column_offset)
    subtract     = setEmptyColumn(df, column_subtract)
    layer        = setEmptyColumn(df, column_layer)
    layer_height = setEmptyColumn(df, column_layer_height)
    # calculate heights
    for column in columns:
        df.loc[df[column].isnull(), column] = (embankment + offset - subtract + layer * layer_height) * factor

# create tiles
def createTiles(tiles_x, tiles_y, map_meta, resolution, epsg_code, logger=None):
    # calculate tile dimensions
    min_x = map_meta['transform'][2]
    max_y = map_meta['transform'][5]
    pixels_x = map_meta['width']
    pixels_y = map_meta['height']
    bounds_width  = pixels_x * resolution
    bounds_height = pixels_y * resolution
    min_y = max_y - bounds_height
    max_x = min_x + bounds_width
    tile_width  = bounds_width / tiles_x
    tile_height = bounds_height / tiles_y
    tile_pixels_x = int(np.ceil(tile_width / resolution))
    tile_pixels_y = int(np.ceil(tile_height / resolution))
    # set tile map meta
    tile_meta = map_meta.copy()
    tile_meta['width']  = tile_pixels_x
    tile_meta['height'] = tile_pixels_y
    # prepare GeoDataFrame
    gdf_tiles = gpd.GeoDataFrame()
    gdf_tiles.insert(len(gdf_tiles.columns), 'tile_x', list(np.full(tiles_x*tiles_y, None)))
    gdf_tiles.insert(len(gdf_tiles.columns), 'tile_y', list(np.full(tiles_x*tiles_y, None)))
    gdf_tiles.insert(len(gdf_tiles.columns), 'tile_transform', list(np.full(tiles_x*tiles_y, None)))
    gdf_tiles.insert(len(gdf_tiles.columns), 'geometry', list(np.full(tiles_x*tiles_y, None)))
    # process each tile
    iloc = 0
    for x_offset in range(0, tiles_x):
        for y_offset in range(0, tiles_y):
            if logger != None:
                logger.info('Processing tile ' + str(x_offset) + ',' + str(y_offset) + '...')
            # create tile
            tile = box(min_x + x_offset * tile_width, min_y + y_offset * tile_height, min_x + (1 + x_offset) * tile_width, min_y + (1 + y_offset) * tile_height)
            # derive tile affine transform
            tile_transform = rasterio.transform.from_origin(int(np.floor((min_x + x_offset * tile_width)/resolution)*resolution), int(np.ceil((min_y + (1 + y_offset) * tile_height)/resolution)*resolution), resolution, resolution)
            # set in gdf
            gdf_tiles.loc[iloc, 'tile_x'] = int(x_offset)
            gdf_tiles.loc[iloc, 'tile_y'] = int(y_offset)
            gdf_tiles.loc[iloc, 'tile_transform'] = tile_transform
            gdf_tiles.loc[iloc, 'geometry'] = tile
            iloc += 1
    gdf_tiles.crs = 'epsg:' + str(epsg_code)
    return gdf_tiles, tile_meta

# convert OSM features to raster/grid
def convertToRaster(map_shape, map_meta, df, value_for_raster, value_for_sorting=None, sort_ascending=True, all_touched=True):
    # re-order GeoDataFrame based on heights (because rasterize writes to raster based on order, overwritting pixels that overlap)
    if value_for_sorting != None:
        df.sort_values(by=value_for_sorting, ascending=sort_ascending, inplace=True)
    else:
        df.sort_values(by=value_for_raster, ascending=sort_ascending, inplace=True)
    # create base map for converting OSM features
    base_map = np.zeros(map_shape)
    # convert vectors to raster
    shapes = df[['geometry', value_for_raster]].values.tolist()
    raster = features.rasterize(shapes=shapes, fill=0, out=base_map, transform=map_meta['transform'], all_touched=all_touched)
    return np.where(raster==0, np.nan, raster)

def convertAllToRaster(shape, map_meta, features):
    # intialize dictionary for storing results
    rasterized_features = {'DTM':{}, 'DSM':{}, 'Manning':{}}
    # process each input entry
    for key in features.keys():
        # initialize arrays (will be used if no features/data are present)
        rasterized_DTM = np.array([])
        rasterized_DSM = np.array([])
        rasterized_n   = np.array([])
        # get single feature type
        feature = features[key]
        if feature != None:
            # get features (GeoDataFrame)
            data = feature['data']
            if not data.empty:
                # get label/column for sorting
                if feature['label_sorting'] == None:
                    label_sorting_DTM = feature['label_DTM']
                    label_sorting_DSM = feature['label_DSM']
                else:
                    label_sorting_DTM = feature['label_sorting']
                    label_sorting_DSM = feature['label_sorting']
                # check if feature should be separated for DTM/DSM
                if feature['separate'] == True:
                    data_for_DTM = data.loc[data['layer']<=0].copy()
                else:
                    data_for_DTM = data.copy()
                # rasterize features
                rasterized_DTM = convertToRaster(shape, map_meta, data_for_DTM, feature['label_DTM'], value_for_sorting=label_sorting_DTM, sort_ascending=feature['sort_ascending'], all_touched=feature['all_touched'])
                rasterized_DSM = convertToRaster(shape, map_meta, data, feature['label_DSM'], value_for_sorting=label_sorting_DSM, sort_ascending=feature['sort_ascending'], all_touched=feature['all_touched'])
                rasterized_n   = convertToRaster(shape, map_meta, data_for_DTM, 'manning', all_touched=feature['all_touched'])
        # store in dictionary
        rasterized_features['DTM'][key]     = rasterized_DTM
        rasterized_features['DSM'][key]     = rasterized_DSM
        rasterized_features['Manning'][key] = rasterized_n
    return rasterized_features

# write raster to file
def writeRasterToFile(raster, path, map_meta, digits=2):
    if map_meta['dtype'] != raster.dtype.name:
        map_meta['dtype'] = raster.dtype.name
    with rasterio.open(path, 'w', **map_meta) as out:
        out.write(np.round(raster, digits), 1)
    out.close()

def writeAllRastersToFile(rasterized_features, path, map_meta, digits=2, filename_add=''):
    DTM_features     = rasterized_features['DTM']
    DSM_features     = rasterized_features['DSM']
    Manning_features = rasterized_features['Manning']
    for key in DTM_features.keys():
        if DTM_features[key].size > 0:
            writeRasterToFile(DTM_features[key], os.path.join(path, key + '_DTM' + filename_add +'.tif'), map_meta, digits)
            writeRasterToFile(DSM_features[key], os.path.join(path, key + '_DSM' + filename_add +'.tif'), map_meta, digits)
            writeRasterToFile(Manning_features[key], os.path.join(path, key + '_Manning' + filename_add +'.tif'), map_meta, 4)

# derive relative elevation maps
def calcRelativeDEM(features, type='DTM', base_val=0):
    # add sidewalks to roads (if any)
    if features[type]['sidewalks'].size > 0:
        features[type]['roads'] = np.fmax.reduce([features[type]['roads'], features[type]['sidewalks']])
    # merge all features together (order of elements only important for Manning's roughness)
    list_relative = [features[type]['buildings'], features[type]['roads'], features[type]['waterways'], features[type]['water_polys']]
    # add features (if any)
    if features[type]['railways'].size > 0:
        list_relative.append(features[type]['railways'])
    if features[type]['embankments'].size > 0:
        list_relative.append(features[type]['embankments'])
    # calculate relative DTM/DSM
    if type == 'DTM':
        rel_DEM = np.fmin.reduce(list_relative)
    elif type == 'DSM':
        rel_DEM = np.fmax.reduce(list_relative)
    elif type == 'Manning':
        # first calculate 'base' map (landuse, natural)
        list_relative_base = []
        if features['Manning']['natural'].size > 0:
            list_relative_base.append(features['Manning']['natural'])
        if features['Manning']['landuse'].size > 0:
            list_relative_base.append(features['Manning']['landuse'])
        rel_DEM = np.fmax.reduce(list_relative_base)
        # then add the other (more detailed) features
        if features['Manning']['aeroways'].size > 0:
            list_relative.insert(0, features['Manning']['aeroways'])
        for roughness_features in list_relative:
            rel_DEM = np.where(np.isnan(roughness_features), rel_DEM, roughness_features)
    # replace NaN with base value
    rel_DEM = np.where(np.isnan(rel_DEM), base_val, rel_DEM)
    return rel_DEM

# merge tiles using rasterio
def mergeTilesRasterio(maindir, DEM_type, map_meta, resolution, digits=2, logger=None):
    logger.info('Merging tiles for ' + DEM_type + '...')
    # constructing list of tiled files
    list_files = []
    for temp_file in os.listdir(os.path.join(maindir, 'tiles', DEM_type)):
        list_files.append(os.path.join(maindir, 'tiles', DEM_type, temp_file))
    to_merge_datasets = [rasterio.open(r, 'r') for r in list_files]
    tiles_merged = rasterio.merge.merge(to_merge_datasets, res=resolution, nodata=map_meta['nodata'])
    tiles_merged = tiles_merged[0][0]
    writeRasterToFile(tiles_merged, os.path.join(maindir, DEM_type + '.tif'), map_meta, digits)

# merge tiles using gdal_merge
def mergeTilesGDAL(maindir, DEM_type, dir_gdal_merge, logger=None):
    logger.info('Merging tiles for ' + DEM_type + '...')
    # constructing list of tiled files
    list_files = []
    for temp_file in os.listdir(os.path.join(maindir, 'tiles', DEM_type)):
        list_files.append(os.path.join(maindir, 'tiles', DEM_type, temp_file))
    # create a text file for input as optfile in gdal_merge command
    path_txt_file = os.path.join(maindir, 'tiles', 'merge_files_' + DEM_type + '.txt')
    if os.path.exists(path_txt_file):
        os.remove(path_txt_file)
    with open(path_txt_file, 'w') as f:
        for temp_value in list_files:
            f.write(str(temp_value) + '\n')
    f.close()
    # merge
    merge_command = ["python", dir_gdal_merge, "-o", os.path.join(maindir, DEM_type + '.tif'), "-ot", "Float32", "-co", "COMPRESS=DEFLATE", "-co", "BIGTIFF=YES", "-n", "nan", "--optfile", path_txt_file]
    subprocess.call(merge_command)
