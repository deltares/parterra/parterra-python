var SRTM = ee.Image("USGS/SRTMGL1_003"),
    ALOS = ee.Image("JAXA/ALOS/AW3D30/V2_2");

// ParTerra DEM pre-processing

// -------------------------------------------------------------------------------------------------- //
// Parameters
// -------------------------------------------------------------------------------------------------- //

// bounds (can be copy+pasted from clipping_boundary.geojson that comes with HOT OSM export)
var bounds = ee.Geometry({"type": "Polygon", "coordinates": [[[4.805174352709207, 52.32012846502451], [4.805174352709207, 52.42849090244252], [5.023870946947488, 52.42849090244252], [5.023870946947488, 52.32012846502451], [4.805174352709207, 52.32012846502451]]]});

// name
var region_name = 'Amsterdam';

// output
var res = 1;     // resolution (m)
var crs = null;  // projection, will be derived automatically when set to null
// var crs = 'EPSG:32631';  // UTM 31 N (Amsterdam)
var incl_crs_in_filename = true;  // when true, projection (EPSG code) is included in name of exported file (includes a 'getInfo')

// Gaussian smoothing
var gaus_radius = 30;
var gaus_sigma  = 15;
var gaus_units  = 'meters';

// Perona Malik smoothing
var pm_iter   = 5;
var pm_K      = 5;
var pm_method = 2;

// number of digits to store
var digits = 2;

// visual parameters
var visParams_DEM = {min:1000, max:1800, palette:['blue','green','yellow','red']};

// -------------------------------------------------------------------------------------------------- //
// DEM pre-processing
// -------------------------------------------------------------------------------------------------- //

// derive UTM zone from region bounds
var getUTMzone = function(bounds) {
  var temp_centroid = bounds.centroid().coordinates();
  var temp_zone_x   = ((ee.Number(temp_centroid.get(0)).add(180)).divide(6)).floor().add(1).format('%d');
  var temp_n_or_s   = ee.Number(1).multiply(ee.Number(temp_centroid.get(1)).lt(0)).add(6).format('%d');
  var temp_epsg     = ee.String('EPSG:32').cat(temp_n_or_s).cat(temp_zone_x);
  return temp_epsg;
};

// Perona Malik filter
// Author: Gennadii Donchyts
var peronaMalikFilter = function(I, iter, K, method) {
  var dxW = ee.Kernel.fixed(3, 3,
                           [[ 0,  0,  0],
                            [ 1, -1,  0],
                            [ 0,  0,  0]]);
  var dxE = ee.Kernel.fixed(3, 3,
                           [[ 0,  0,  0],
                            [ 0, -1,  1],
                            [ 0,  0,  0]]);
  var dyN = ee.Kernel.fixed(3, 3,
                           [[ 0,  1,  0],
                            [ 0, -1,  0],
                            [ 0,  0,  0]]);
  
  var dyS = ee.Kernel.fixed(3, 3,
                           [[ 0,  0,  0],
                            [ 0, -1,  0],
                            [ 0,  1,  0]]);
  var lambda = 0.2;
  var k1 = ee.Image(-1.0/K);
  var k2 = ee.Image(K).multiply(ee.Image(K));
  for(var i = 0; i < iter; i++) {
    var dI_W = I.convolve(dxW);
    var dI_E = I.convolve(dxE);
    var dI_N = I.convolve(dyN);
    var dI_S = I.convolve(dyS);
    switch(method) {
      case 1:
        var cW = dI_W.multiply(dI_W).multiply(k1).exp();
        var cE = dI_E.multiply(dI_E).multiply(k1).exp();
        var cN = dI_N.multiply(dI_N).multiply(k1).exp();
        var cS = dI_S.multiply(dI_S).multiply(k1).exp();
        I = I.add(ee.Image(lambda).multiply(cN.multiply(dI_N).add(cS.multiply(dI_S)).add(cE.multiply(dI_E)).add(cW.multiply(dI_W))));
        break;
      case 2:
        var cW = ee.Image(1.0).divide(ee.Image(1.0).add(dI_W.multiply(dI_W).divide(k2)));
        var cE = ee.Image(1.0).divide(ee.Image(1.0).add(dI_E.multiply(dI_E).divide(k2)));
        var cN = ee.Image(1.0).divide(ee.Image(1.0).add(dI_N.multiply(dI_N).divide(k2)));
        var cS = ee.Image(1.0).divide(ee.Image(1.0).add(dI_S.multiply(dI_S).divide(k2)));
        I = I.add(ee.Image(lambda).multiply(cN.multiply(dI_N).add(cS.multiply(dI_S)).add(cE.multiply(dI_E)).add(cW.multiply(dI_W))));
        break;
    }
  }
  return I;
};

// prep DEM
function prep_DEM(img, name) {
  // show base DEMs on map
  // Map.addLayer(img, visParams_DEM, name, false);
  // first, apply gaussian filter with small kernel
  var DEM_smooth_1 = img.convolve(ee.Kernel.gaussian(gaus_radius, gaus_sigma, gaus_units));
  // Map.addLayer(DEM_smooth_1, visParams_DEM, name + ' (smoothing step 1)', false);
  // then, apply the Perona Malik filter
  var DEM_smooth_2 = peronaMalikFilter(DEM_smooth_1, pm_iter, pm_K, pm_method);
  // Map.addLayer(DEM_smooth_2, visParams_DEM, name + ' (smoothing step 2)', false);
  // finally, reproject, clip to bounds and set numerical precision (digits)
  var DEM_resampled = DEM_smooth_2.resample('bicubic').reproject(crs, null, res).clip(bounds);
  var digits_factor = ee.Number(10).pow(digits);
  DEM_resampled = DEM_resampled.multiply(digits_factor).round().divide(digits_factor);
  // Map.addLayer(DEM_resampled, visParams_DEM, name + ' (smoothing final)', false);
  return DEM_resampled;
}

// get UTM zone
if (crs === null) {
  var crs = getUTMzone(bounds);
  print('using auto derived crs:', crs);
} else {
  var crs_auto = getUTMzone(bounds);
  print('auto derived crs:', crs_auto);
  print('using manually set crs:', crs);
}

// prep DEMs
var SRTM_prep = prep_DEM(SRTM, 'SRTM');
var ALOS_prep = prep_DEM(ALOS.select('AVE_DSM'), 'ALOS');

// -------------------------------------------------------------------------------------------------- //
// Exports
// -------------------------------------------------------------------------------------------------- //

var descr_add = '';
if (incl_crs_in_filename) {
  descr_add += '_' + crs.getInfo().replace(':','');
}

var exportRaw = function(img, name) {
  Export.image.toDrive({
    image: img,
    description: name + '_' + region_name + '_30m' + descr_add,
    // folder: '',
    // fileNamePrefix: '',
    // dimensions: ,
    region: bounds,
    scale: 30,
    crs: crs,
    // crsTransform: ,
    // maxPixels: 
  });
};

var exportPrep = function(img, name) {
  Export.image.toDrive({
    image: img,
    description: name + '_' + region_name + '_' + res.toString().replace('.','') + 'm' + descr_add,
    // folder: '',
    // fileNamePrefix: '',
    // dimensions: ,
    region: bounds,
    scale: res,
    crs: crs,
    // crsTransform: ,
    // maxPixels: 
  });
};

// raw DEMs
// exportRaw(SRTM, 'SRTM');
// exportRaw(ALOS, 'ALOS');

// pre-processed DEMs
exportPrep(SRTM_prep, 'SRTM');
exportPrep(ALOS_prep, 'ALOS');

// bounds
// Export.table.toDrive({
//   collection: ee.FeatureCollection(ee.Feature(bounds)),
//   description: 'bounds_' + region_name,
//   folder: '',
//   // fileNamePrefix: '',
//   fileFormat: 'GeoJSON',
//   // selectors:
// });

// -------------------------------------------------------------------------------------------------- //
// Map
// -------------------------------------------------------------------------------------------------- //

Map.centerObject(bounds);

Map.addLayer(ee.Image().byte().paint(bounds, 0, 2), {}, 'bounds', true);
