# -*- coding: utf-8 -*-
"""
Handles logging for ParTerra.

@author: haag (arjen.haag@deltares.nl)
"""

import os
import sys
import logging


# main logging setup
def setupLogging(logPath=None, name='default'):
    """
    Setup/config of logging procedures.
    
    Parameters:
        logPath : [string] path specifying where to store logging file
            
    Output: instance of Python logging module extended with counting functionality
    """
    # setup path/file
    if logPath == None:
        logPath = os.getcwd()
    if os.path.exists(os.path.join(logPath, 'log.txt')):
        os.remove(os.path.join(logPath, 'log.txt'))
    # setup basic logging configuration
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s [%(levelname)-8.8s]  %(message)s",
        handlers=[
            logging.FileHandler("{0}/{1}.txt".format(logPath, 'log')),
            logging.StreamHandler()
        ])
    # decorator to determine number of calls for a method (https://stackoverflow.com/questions/812477/how-many-times-was-logging-error-called)
    class CallCounted:
        def __init__(self,method):
            self.method=method
            self.counter=0
        def __call__(self,*args,**kwargs):
            self.counter+=1
            return self.method(*args,**kwargs)
    # create a logger
    logger = logging.getLogger(name)
    # extend with decorator
    logger.info    = CallCounted(logger.info)
    logger.warning = CallCounted(logger.warning)
    logger.error   = CallCounted(logger.error)
    logger.fatal   = CallCounted(logger.fatal)
    return logger


# statistics
def getStats(logger):
    log_warns  = logger.warning.counter
    log_errors = logger.error.counter
    #log_fatal  = logger.fatal.counter
    logger.info('Checking logging statistics...')
    logger.info('warning:  ' + str(log_warns))
    logger.info('error:    ' + str(log_errors))
    #logger.info('fatal:    ' + str(log_fatal))
    #return {'warn':log_warns, 'err':log_errors, 'crit':log_fatal}


# close logger
def closeLogging():
    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)
