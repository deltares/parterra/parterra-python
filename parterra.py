# -*- coding: utf-8 -*-
"""
Handles all processing for ParTerra.

@author: haag (arjen.haag@deltares.nl)
"""


import os
import sys
import configparser as cp

import numpy as np
import pandas as pd
import geopandas as gpd
import rasterio
import rasterio.mask
import rasterio.merge

import scipy
import scipy.ndimage

import utils
import dem_prep
import create_figures
import handle_logging


# get settings file
if len(sys.argv) > 1:
    settings_file = sys.argv[1]
else:
    settings_file = 'settings.ini'

# read settings
config = cp.ConfigParser()
try:
    config.read(settings_file)
except:
    sys.exit("ERROR: Not possible to open settings file.")

if config.has_section("STRUCTURE"):
    path_input       = utils.check_key(config["STRUCTURE"],"path_input", os.getcwd())
    dir_osm          = utils.check_key(config["STRUCTURE"],"dir_osm", 'data')
    lines_file       = utils.check_key(config["STRUCTURE"],"lines_file", 'osm_lines_lines.shp')
    polys_file       = utils.check_key(config["STRUCTURE"],"polys_file", 'osm_polys_polygons.shp')
    bounds_file      = utils.check_key(config["STRUCTURE"],"bounds_file", 'clipping_boundary.geojson')
    dir_dem          = utils.check_key(config["STRUCTURE"],"dir_dem", '')
    dem_file         = utils.check_key(config["STRUCTURE"],"dem_file", 'DEM.tif')
    dem_file_for_DTM = utils.check_key(config["STRUCTURE"],"dem_file_for_DTM")
    dem_file_for_DSM = utils.check_key(config["STRUCTURE"],"dem_file_for_DSM")
    dir_filters      = utils.check_key(config["STRUCTURE"],"dir_filters", 'filters')
    path_output      = utils.check_key(config["STRUCTURE"],"path_output", os.getcwd())
    dir_shapes_out   = utils.check_key(config["STRUCTURE"],"dir_shapes_out", 'output_shapefiles')
    dir_rasters_out  = utils.check_key(config["STRUCTURE"],"dir_rasters_out", 'output_rasters')
    dir_gdal_merge   = utils.check_key(config["STRUCTURE"],"dir_gdal_merge")
if config.has_section("CONTROLS"):
    do_sidewalks        = bool(int(utils.check_key(config["CONTROLS"],"do_sidewalks", 1)))
    do_straightening    = bool(int(utils.check_key(config["CONTROLS"],"do_straightening", 0)))
    do_figures          = bool(int(utils.check_key(config["CONTROLS"],"do_figures", 1)))
    do_shapefiles       = bool(int(utils.check_key(config["CONTROLS"],"do_shapefiles", 0)))
    do_feature_maps     = bool(int(utils.check_key(config["CONTROLS"],"do_feature_maps", 0)))
    do_relative_maps    = bool(int(utils.check_key(config["CONTROLS"],"do_relative_maps", 0)))
    do_manning          = bool(int(utils.check_key(config["CONTROLS"],"do_manning", 0)))
if config.has_section("PARS"):
    target_res                 = float(utils.check_key(config["PARS"],"resolution", 1))
    epsg_code                  = utils.check_key(config["PARS"],"epsg_code")
    digits                     = int(utils.check_key(config["PARS"],"digits", 2))
    tiles_x                    = utils.check_key(config["PARS"],"tiles_x")
    tiles_y                    = utils.check_key(config["PARS"],"tiles_y")
    gaus_sigma                 = int(utils.check_key(config["PARS"],"gaussian_sigma", 15))
    pm_iter                    = int(utils.check_key(config["PARS"],"pm_iterations", 5))
    pm_k                       = int(utils.check_key(config["PARS"],"pm_kappa", 5))
    sea_point                  = utils.check_key(config["PARS"],"sea_point")
    all_touched                = bool(int(utils.check_key(config["PARS"],"all_touched", 0)))
    min_tunnel_layer           = int(utils.check_key(config["PARS"],"min_tunnel_layer", -3))
    max_tunnel_length          = float(utils.check_key(config["PARS"],"max_tunnel_length", 200.0))
    building_DTM_label         = utils.check_key(config["PARS"],"building_DTM_label", "threshold")
    man_made_embankment_types  = utils.check_key(config["PARS"],"man_made_embankment_types", "levee,dike,dyke,embankment,reinforced_slope")
    remove_waterway_types      = utils.check_key(config["PARS"],"remove_waterway_types","dam,weir,lock_gate,milestone,boatyard,water_point,check_dam,sluice_gate")
    remove_railway_types       = utils.check_key(config["PARS"],"remove_railway_types", "station,traverser,razed,dismantled,historic,abandoned,platform,tram_stop")
    remove_aeroway_types       = utils.check_key(config["PARS"],"remove_aeroway_types", "navigationaid,gate,windsock,landing_light,marking,waypoint,light,approach_light,beacon")
    not_elevated_road_types    = utils.check_key(config["PARS"],"not_elevated_road_types")
    not_elevated_road_surfaces = utils.check_key(config["PARS"],"not_elevated_road_surfaces")

# construct and check paths
path_OSM = os.path.join(path_input, dir_osm)
path_DEM = os.path.join(path_input, dir_dem)
if not os.path.exists(path_OSM):
    sys.exit("ERROR: specified path to OSM data does not exist!")
if not os.path.exists(path_DEM):
    sys.exit("ERROR: specified path to DEM data does not exist!")

# convert relevant entries to lists
if sea_point != None:
    sea_point = list(map(int, sea_point.split(',')))
man_made_embankment_types  = utils.convertStringToList(man_made_embankment_types)
remove_waterway_types      = utils.convertStringToList(remove_waterway_types)
remove_railway_types       = utils.convertStringToList(remove_railway_types)
remove_aeroway_types       = utils.convertStringToList(remove_aeroway_types)
not_elevated_road_types    = utils.convertStringToList(not_elevated_road_types)
not_elevated_road_surfaces = utils.convertStringToList(not_elevated_road_surfaces)

# get logger
logger = handle_logging.setupLogging(path_output)
logger.info('Starting ParTerra processing...')
logger.info('Including sidewalks:             ' + str(do_sidewalks))
logger.info('Straightening base DEM:          ' + str(do_straightening))
logger.info('Creating figures of OSM data:    ' + str(do_figures))
logger.info('Creating shapefiles of OSM data: ' + str(do_shapefiles))
logger.info('Creating rasterized OSM data:    ' + str(do_feature_maps))
logger.info('Creating relative height maps:   ' + str(do_relative_maps))
logger.info('Creating Manning roughness map:  ' + str(do_relative_maps))

# checking DEM straightening settings
use_previously_straightened = False
if dem_file_for_DTM != None and dem_file_for_DSM != None:
    if os.path.exists(os.path.join(path_DEM, dem_file_for_DTM)) and os.path.exists(os.path.join(path_DEM, dem_file_for_DSM)):
        use_previously_straightened = True
        if do_straightening:
            logger.warning('Straightening base DEM but previously straightened DEMs were also provided! Using previously straightened DEMs instead of carrying out new straightening procedure!')
            do_straightening = False
    else:
        if do_straightening:
            logger.warning('Straightening base DEM but previously straightened DEMs were also provided! Could not find previously straightened DEMs so carrying out new straightening procedure!')
        else:
            logger.error('Previously straightened DEMs provided but could not be found! Using base DEM instead!')

# read filters
filter_buildings   = pd.read_csv(os.path.join(dir_filters, 'building.csv'))
filter_roads       = pd.read_csv(os.path.join(dir_filters, 'highway.csv'))
filter_railways    = pd.read_csv(os.path.join(dir_filters, 'railway.csv'))
filter_embankments = pd.read_csv(os.path.join(dir_filters, 'embankment.csv'))
filter_waterways   = pd.read_csv(os.path.join(dir_filters, 'waterway.csv'))
filter_water       = pd.read_csv(os.path.join(dir_filters, 'water.csv'))
filter_natural     = pd.read_csv(os.path.join(dir_filters, 'natural.csv'))
filter_landuse     = pd.read_csv(os.path.join(dir_filters, 'landuse.csv'))
filter_aeroways    = pd.read_csv(os.path.join(dir_filters, 'aeroway.csv'))

# get keywords from filters
building_levels_key = filter_buildings.columns[3]
if '_' in building_levels_key:
    building_material_key = 'building_m'
else:
    building_material_key = 'buildingma'

# get user-specified features to remove (e.g. certain tunnels)
osm_ids_to_remove_lines = pd.read_csv(os.path.join(dir_filters, 'remove.csv'), dtype=str, usecols=['lines'])
osm_ids_to_remove_polys = pd.read_csv(os.path.join(dir_filters, 'remove.csv'), dtype=str, usecols=['polygons'])
osm_ids_to_remove_lines.dropna(inplace=True)
osm_ids_to_remove_polys.dropna(inplace=True)
osm_ids_to_remove_lines = osm_ids_to_remove_lines.values.flatten()
osm_ids_to_remove_polys = osm_ids_to_remove_polys.values.flatten()

# get user-specified corrections/replacements to be applied on OSM data
filter_replace = pd.read_csv(os.path.join(dir_filters, 'replace.csv'), dtype=str)
filter_replace = dict(filter_replace.replace({np.nan:''}).to_numpy())

# read OSM data
logger.info('Reading OSM lines data from ' + os.path.join(path_OSM, lines_file))
logger.info('Reading OSM polygons data from ' + os.path.join(path_OSM, polys_file))
df_lines  = gpd.read_file(os.path.join(path_OSM, lines_file))
df_polys  = gpd.read_file(os.path.join(path_OSM, polys_file))
df_bounds = gpd.read_file(os.path.join(path_OSM, bounds_file))

# prep for tiles
do_tiles = False
if (tiles_x != None) and (tiles_y != None):
    do_tiles = True
    # convert to integers
    if tiles_x != None:
        tiles_x = int(tiles_x)
    if tiles_y != None:
        tiles_y = int(tiles_y)
    # create folders for storing intermediate tile files
    if not os.path.exists(os.path.join(path_output, dir_rasters_out, 'tiles')):
        os.mkdir(os.path.join(path_output, dir_rasters_out, 'tiles'))
    if not os.path.exists(os.path.join(path_output, dir_rasters_out, 'tiles', 'DTM')):
        os.mkdir(os.path.join(path_output, dir_rasters_out, 'tiles', 'DTM'))
    if not os.path.exists(os.path.join(path_output, dir_rasters_out, 'tiles', 'DSM')):
        os.mkdir(os.path.join(path_output, dir_rasters_out, 'tiles', 'DSM'))
    if do_manning:
        if not os.path.exists(os.path.join(path_output, dir_rasters_out, 'tiles', 'Manning')):
            os.mkdir(os.path.join(path_output, dir_rasters_out, 'tiles', 'Manning'))

# read base DEM, preprocess if needed
if not use_previously_straightened:
    logger.info('Reading base DEM from ' + os.path.join(path_DEM, dem_file))
    # get base DEM and metadata
    src = rasterio.open(os.path.join(path_DEM, dem_file))
    base_DEM = src.read(1)
    map_meta = src.meta
    map_meta.update(compress='lzw')
    resolution = abs(map_meta['transform'][0])
    # check projection (EPSG code)
    epsg_code = utils.checkEPSG(df_bounds, epsg_code, map_meta['crs'].to_epsg(), logger)
    # get tiles
    if do_tiles:
        logger.info('Constructing tiles...')
        df_tiles, tile_meta = utils.createTiles(tiles_x, tiles_y, map_meta, resolution, epsg_code)
        if do_shapefiles:
            logger.info('Saving tiles to ' + os.path.join(path_output, dir_shapes_out, 'tiles.shp'))
            df_tiles[['tile_x','tile_y','geometry']].to_file(os.path.join(path_output, dir_shapes_out, 'tiles.shp'))
    if resolution != target_res:
        logger.info('Resampling and smoothing DEM from ' + str(abs(map_meta['transform'][0])) + ' m to ' + str(target_res) + ' m')
        base_DEM = dem_prep.prepDEM(src, base_DEM, path_output, dir_rasters_out, target_res=target_res, gaus_sigma=gaus_sigma, pm_iter=pm_iter, pm_k=pm_k, file_temp_DEM=dem_file.split('.')[0], logger=logger)
        map_meta = base_DEM[1]
        base_DEM = base_DEM[0]
        resolution = abs(map_meta['transform'][0])
        src.close()
        if do_straightening or do_tiles:
            utils.writeRasterToFile(base_DEM, os.path.join(path_DEM, dem_file.split('.')[0] + '_prep.tif'), map_meta)
            src = rasterio.open(os.path.join(path_DEM, dem_file.split('.')[0] + '_prep.tif'))
    else:
        if do_straightening or do_tiles:
            pass
        else:
            src.close()
    base_shape = base_DEM.shape
else:
    logger.info('Reading previously straightened DEM for DTM from ' + os.path.join(path_DEM, dem_file_for_DTM))
    src_1 = rasterio.open(os.path.join(path_DEM, dem_file_for_DTM))
    DEM_straightened_for_DTM = src_1.read(1)
    map_meta = src_1.meta
    map_meta.update(compress='lzw')
    resolution = abs(map_meta['transform'][0])
    epsg_code = utils.checkEPSG(df_bounds, epsg_code, map_meta['crs'].to_epsg(), logger)
    base_shape = DEM_straightened_for_DTM.shape
    logger.info('Reading previously straightened DEM for DSM from ' + os.path.join(path_DEM, dem_file_for_DSM))
    src_2 = rasterio.open(os.path.join(path_DEM, dem_file_for_DSM))
    DEM_straightened_for_DSM = src_2.read(1)
    if do_tiles:
        logger.info('Constructing tiles...')
        df_tiles, tile_meta = utils.createTiles(tiles_x, tiles_y, map_meta, resolution, epsg_code)
        if do_shapefiles:
            logger.info('Saving tiles to ' + os.path.join(path_output, dir_shapes_out, 'tiles.shp'))
            df_tiles[['tile_x','tile_y','geometry']].to_file(os.path.join(path_output, dir_shapes_out, 'tiles.shp'))
    if resolution != target_res:
        logger.warning('Resolution of previously straightened DEMs (' + str(resolution) + ' m) does not equal target resolution (' + str(target_res) + ' m)!')
    else:
        src_1.close()
        src_2.close()

# convert OSM data to specified projection
logger.info('Reprojecting input data to EPSG:'+str(epsg_code))
df_lines  = df_lines.to_crs(epsg=epsg_code)
df_polys  = df_polys.to_crs(epsg=epsg_code)
df_bounds = df_bounds.to_crs(epsg=epsg_code)

# get/show some info on data
if do_figures:
    create_figures.subPlots(rows=1, cols=2, dfs=[df_lines, df_polys], base_titles=['OSM lines', 'OSM polygons'], path=path_output, filename='ParTerra_OSM_features')
    create_figures.histograms(rows=1, cols=2, dfs=[df_lines, df_polys], titles=['Lines', 'Polygons'], path=path_output, filename='ParTerra_OSM_properties_by_features')

# merge osm_way_id and osm_id attributes (only required on older OSM exports, recent data only has osm_id?)
if 'osm_way_id' in df_polys.columns:
    df_polys.loc[(df_polys['osm_id'].isnull()) & (~df_polys['osm_way_id'].isnull()), 'osm_id'] = df_polys.loc[(df_polys['osm_id'].isnull()) & (~df_polys['osm_way_id'].isnull()), 'osm_way_id']

# convert OSM id's to strings
df_lines['osm_id'] = df_lines['osm_id'].astype(int).astype(str)
df_polys['osm_id'] = df_polys['osm_id'].astype(int).astype(str)

# correct or remove incorrect entries
logger.info('Replacing incorrect OSM entries...')
utils.applyCorrections(df_lines, ['width','layer','depth'], 'lines', filter_replace)
utils.applyCorrections(df_polys, [building_levels_key,'depth','height'], 'polygons', filter_replace)

# convert certain existing columns to numeric type
logger.info('Converting entries to numeric type...')
utils.convertToNumeric(df_lines, ['width','layer','depth'], 'lines')
utils.convertToNumeric(df_polys, [building_levels_key,'depth','height'], 'polygons')

# create output dir for shapefiles
if do_shapefiles:
    if not os.path.exists(os.path.join(path_output, dir_shapes_out)):
        os.mkdir(os.path.join(path_output, dir_shapes_out))

# remove long/deep tunnels and connected tunnels
tunnels = df_lines[(df_lines['tunnel'].notnull()) & (df_lines['tunnel']!='no') & (~df_lines['waterway'].isin(['river','canal','stream','riverbank']))].copy()
tunnels['length'] = tunnels.geometry.length
tunnels_for_removal = tunnels.loc[(tunnels['length']>=max_tunnel_length) | (tunnels['layer'] <= min_tunnel_layer)]
tunnels_for_removal.insert(len(tunnels_for_removal.columns), 'remove_iter', list(np.full(len(tunnels_for_removal.index), 0)))
logger.info('Found ' + str(len(tunnels_for_removal)) + ' tunnels for removal (too long and/or too deep)')
if do_figures and len(tunnels_for_removal) > 0:
    create_figures.singlePlot(tunnels_for_removal, show_count=True, base_title='Long/deep tunnels to be removed', path=path_output, filename='ParTerra_lines_removed_auto_1')
tunnels = tunnels.loc[(tunnels['tunnel']!='culvert') & ((tunnels['highway'].isnull()) | (tunnels['highway'].isin(['footway','steps','unclassified','pedestrian','platform','elevator','yes'])))]
iter_count = 1
while len(tunnels_for_removal.loc[tunnels_for_removal['remove_iter']==(iter_count-1)]) > 0:
    logger.info('Searching for connected tunnels for removal, iteration count: ' + str(iter_count))
    found_ids = tunnels_for_removal.loc[tunnels_for_removal['remove_iter']<iter_count, 'osm_id'].tolist()
    tunnels_for_removal_2  = tunnels.loc[(~tunnels['osm_id'].isin(found_ids)) & ((tunnels.touches(tunnels_for_removal.unary_union)) | (tunnels.intersects(tunnels_for_removal.unary_union)))]
    tunnels_for_removal_2.insert(len(tunnels_for_removal_2.columns), 'remove_iter', list(np.full(len(tunnels_for_removal_2.index), iter_count)))
    logger.info('Found ' + str(len(tunnels_for_removal_2)) + ' connected tunnels for removal')
    tunnels_for_removal = tunnels_for_removal.append(tunnels_for_removal_2, ignore_index=True, sort=False)
    iter_count += 1
logger.info('Final tunnel removal count: ' + str(len(tunnels_for_removal)))
if do_shapefiles:
    tunnels_for_removal.to_file(os.path.join(path_output, dir_shapes_out, 'tunnels_for_removal.shp'))
if do_figures and len(tunnels_for_removal) > 0:
    create_figures.singlePlot(tunnels_for_removal, show_count=True, base_title='Removed tunnel line objects (auto)', path=path_output, filename='ParTerra_lines_removed_auto_2')
df_lines = df_lines.loc[~df_lines['osm_id'].isin(tunnels_for_removal['osm_id'].tolist())]

# remove user-specified features
logger.info('Checking user-specified features to remove...')
if len(osm_ids_to_remove_lines) > 0:
    if len(df_lines.loc[df_lines['osm_id'].isin(osm_ids_to_remove_lines)]) > 0:
        logger.info('Removing ' + str(len(df_lines.loc[df_lines['osm_id'].isin(osm_ids_to_remove_lines)])) + ' user-specified line features...')
        if do_figures:
            create_figures.singlePlot(df_lines[df_lines['osm_id'].isin(osm_ids_to_remove_lines)], show_count=True, base_title='Removed line objects (user-specified)', path=path_output, filename='ParTerra_lines_removed_user')
        if do_shapefiles:
            df_lines.loc[df_lines['osm_id'].isin(osm_ids_to_remove_lines)].to_file(os.path.join(path_output, dir_shapes_out, 'tunnels_for_removal_user_lines.shp'))
        df_lines = df_lines[~df_lines['osm_id'].isin(osm_ids_to_remove_lines)]
    else:
        logger.warning("No user-specified line features to remove were found, please check the ID's! (could also have been removed already with automatic procedure)")
if len(osm_ids_to_remove_polys) > 0:
    if len(df_polys.loc[df_polys['osm_id'].isin(osm_ids_to_remove_polys)]) > 0:
        logger.info('Removing ' + str(len(df_polys.loc[df_polys['osm_id'].isin(osm_ids_to_remove_polys)])) + ' user-specified polygon features...')
        if do_figures:
            create_figures.singlePlot(df_polys[df_polys['osm_id'].isin(osm_ids_to_remove_polys)], show_count=True, base_title='Removed polygons objects (user-specified)', path=path_output, filename='ParTerra_polys_removed_user')
        if do_shapefiles:
            df_polys.loc[df_polys['osm_id'].isin(osm_ids_to_remove_polys)].to_file(os.path.join(path_output, dir_shapes_out, 'tunnels_for_removal_user_polys.shp'))
        df_polys = df_polys[~df_polys['osm_id'].isin(osm_ids_to_remove_polys)]
    else:
        logger.warning("No user-specified polygon features to remove were found, please check the ID's!")

# select only relevant features (filter out those containing missing or unworkable values)
# NOTE: there are two options here:
#       (1) filter only features that are "100%" sure water, by only taking those with 'natural=water' that have null values at the other water columns, or
#       (2) filter all features that are potentially water, by taking all with 'natural=water'. This is handled in the code block below with the last two lines (so make sure one is commented out!).
#       In the example used here, (1) will yield 10 water features while (2) yields 11, with one having 'natural=water' but also 'landuse=grass'.
#       What would be the best option?! It would be easy to use (1), but this assumes that all relevant key/value combinations of 'water', 'waterway' and 'landuse' are included in the filter already...
#       which might not be the case?
logger.info('Splitting up lines and polygons into specific types (e.g. buildings, roads, waterways)...')
buildings   = df_polys[df_polys['building'].notnull()].copy()
aeroways    = df_polys[df_polys['aeroway'].notnull()].copy()
roads       = df_lines[df_lines['highway'].notnull()].copy()
railways    = df_lines[df_lines['railway'].notnull()].copy()
coastlines  = df_lines[df_lines['natural']=='coastline'].copy()
waterways   = df_lines[df_lines['waterway'].notnull()].copy()
tunnels     = df_lines[df_lines['tunnel'].notnull()].copy()
water_polys = df_polys[(df_polys['water'].isin(['river', 'stream', 'canal', 'lake', 'pond', 'lock', 'reservoir'])) | \
                       (df_polys['waterway'].isin(['river', 'stream', 'canal', 'riverbank', 'ditch', 'drain'])) | \
                       (df_polys['landuse'].isin(['reservoir', 'basin'])) | \
                       (df_polys['natural']=='water')].copy() # either this or next line
                       #((df_polys['natural']=='water') & (df_polys['water'].isnull()) & (df_polys['waterway'].isnull()) & (df_polys['landuse'].isnull()))].copy() # either this or previous line
natural     = df_polys[df_polys['natural'].notnull() & (~df_polys['natural'].isin(['water']))].copy()
landuse     = df_polys[df_polys['landuse'].notnull() & (~df_polys['landuse'].isin(['reservoir', 'basin']))].copy()
if 'embankment' in df_lines.columns:
    embankments = df_lines[(df_lines['embankment'].notnull()) & (df_lines['embankment']!='no') | (df_lines['man_made'].isin(man_made_embankment_types))].copy()
    # convert embankment types
    embankments.loc[~embankments['embankment'].isin(man_made_embankment_types), 'embankment'] = 'embankment'
else:
    if 'man_made' in df_lines.columns:
        embankments = df_lines[df_lines['man_made'].isin(man_made_embankment_types)].copy()
    else:
        embankments = pd.DataFrame({}, index=[], columns=['highway', 'railway', 'waterway'])

# remove non-relevant features (for railways, would be good if some of these are included later on, such as platform/tram_stop, but compare with railway polygons first)
logger.info('Removing irrelevant features...')
logger.info('Removed railway features: ' + str(len(railways.loc[railways['railway'].isin(remove_railway_types)])))
railways = railways[~railways['railway'].isin(remove_railway_types)]
embankments = embankments[~embankments['railway'].isin(remove_railway_types)]
logger.info('Removed aeroway features: ' + str(len(aeroways.loc[aeroways['aeroway'].isin(remove_aeroway_types)])))
aeroways = aeroways[~aeroways['aeroway'].isin(remove_aeroway_types)]
logger.info('Removed waterway features: ' + str(len(waterways.loc[waterways['waterway'].isin(remove_waterway_types)])))
waterways = waterways[~waterways['waterway'].isin(remove_waterway_types)]

# set water polygon to use a single property, with hierarchy: 1. water, 2. waterway, 3. landuse, 4. natural
logger.info('Merging water polygon attributes from different features to a single attribute...')
water_polys = water_polys.fillna({'water':water_polys['waterway']}).fillna({'water':water_polys['landuse']}).fillna({'water':water_polys['natural']})

# add sea polygon(s)
if sea_point != None:
    logger.info('Obtaining sea polygon from coastlines and bounding box...')
    sea_poly    = utils.getSeaPolygon(coastlines, sea_point, df_bounds, water_polys.columns.values, logger)
    logger.info('Adding sea polygon to water polygons...')
    water_polys = water_polys.append(sea_poly, ignore_index=True, sort=False)

# convert waterway(s) with closed linear-ring type geometry to water polygon(s)
waterways_polys = utils.convertRingsToPolys(waterways)
logger.info('Found ' + str(len(waterways_polys)) + ' waterway(s) with LinearRing geometries for conversion to polygon(s)')
if not waterways_polys.empty:
    if do_shapefiles:
        waterways_polys.to_file(os.path.join(path_output, dir_shapes_out, 'waterways_LinearRings.shp'))
    water_polys = water_polys.append(waterways_polys, ignore_index=True, sort=False)
    water_polys = water_polys.fillna({'water':water_polys['waterway']})
    waterways = waterways[~waterways['osm_id'].isin(waterways_polys['osm_id'].values)]

# update buildings with more detailed aeroways information (and remove those features from aeroways)
aeroway_building_ids = buildings.loc[(buildings['building']=='yes') & (~buildings['aeroway'].isnull()),'osm_id']
if not aeroway_building_ids.empty:
    aeroway_building_ids = aeroway_building_ids.values
    logger.info('Found ' + str(len(aeroway_building_ids)) + ' aeroway features which will be processed as buildings')
    buildings.loc[buildings['osm_id'].isin(aeroway_building_ids), 'building'] = buildings.loc[buildings['osm_id'].isin(aeroway_building_ids), 'aeroway']
    aeroways = aeroways.loc[~aeroways['osm_id'].isin(aeroway_building_ids)]

# remove irrelevant columns
logger.info('Removing irrelevant columns from features...')
utils.removeColumns(buildings, ['aeroway','embankment','covered','smoothness','blockage'])
utils.removeColumns(roads, ['diameter','railway','natural','aeroway','blockage'])
utils.removeColumns(railways, ['diameter','highway','natural','aeroway','blockage'])
utils.removeColumns(waterways, ['railway','highway','aeroway','is_ring'])
utils.removeColumns(embankments, ['diameter','blockage'])
utils.removeColumns(aeroways, [building_levels_key,building_material_key,'building','embankment','public_tra'])
utils.removeColumns(natural, [building_levels_key,building_material_key,'building','public_tra','covered','man_made','blockage'])
utils.removeColumns(landuse, [building_levels_key,building_material_key,'building','public_tra','covered','man_made','blockage'])
utils.removeColumns(water_polys, [building_levels_key,building_material_key,'building','public_tra','railway','aeroway','highway','diameter','width','is_ring'])

# figures of grouped features (before application of filters)
if do_figures:
    create_figures.histograms(rows=3, cols=2, dfs=[ roads,   railways,   waterways,   buildings,   water_polys, aeroways], \
                                           titles=['roads', 'railways', 'waterways', 'buildings', 'water',     'aeroways'], path=path_output, filename='ParTerra_OSM_properties_by_features_filtered')

# create new (empty) columns for relevant properties in filters
logger.info('Adding empty columns...')
utils.addNewColumns(buildings, filter_buildings, 'buildings')
utils.addNewColumns(roads, filter_roads, 'roads')
utils.addNewColumns(railways, filter_railways, 'railways')
utils.addNewColumns(embankments, filter_embankments, 'embankments')
utils.addNewColumns(waterways, filter_waterways, 'waterways')
utils.addNewColumns(water_polys, filter_water, 'water polygons')
utils.addNewColumns(natural, filter_natural, 'natural')
utils.addNewColumns(landuse, filter_landuse, 'landuse')
utils.addNewColumns(aeroways, filter_aeroways, 'aeroway')

# then, fill them with relevant values
logger.info('Assigning values to columns...')
utils.assignColumnValues(buildings, filter_buildings, 'building', [building_levels_key,'level_height','threshold','manning'])
utils.assignColumnValues(roads, filter_roads, 'highway', ['width','layer','max_width','drive_frac','layer_height','driveway_offset','sidewalk_offset','embank_height','manning'])
utils.assignColumnValues(railways, filter_railways, 'railway', ['width','max_width','layer','layer_height','offset','embank_height','manning'])
utils.assignColumnValues(embankments, filter_embankments, 'embankment', ['overrule_infra','width','height','max_width','max_height','manning'])
utils.assignColumnValues(waterways, filter_waterways, 'waterway', ['width','depth','max_width','max_depth','manning'])
utils.assignColumnValues(water_polys, filter_water, 'water', ['depth','max_depth','manning'])
utils.assignColumnValues(natural, filter_natural, 'natural', ['height','manning'])
utils.assignColumnValues(landuse, filter_landuse, 'landuse', ['height','manning'])
utils.assignColumnValues(aeroways, filter_aeroways, 'aeroway', ['height','manning'])

# restrict certain entries to maximum allowed values
logger.info('Capping column values to maximum allowed values...')
utils.capMax(roads, ['width'], 'roads')
utils.capMax(railways, ['width'], 'railways')
utils.capMax(embankments, ['width', 'height'], 'embankments')
utils.capMax(waterways, ['width', 'depth'], 'waterways')
utils.capMax(water_polys, ['depth'], 'water polygons')

# set embankments for infrastructure features
utils.setEmbankments(roads, embankments, 'highway', exclude_embankment_types=man_made_embankment_types, logger=logger)
utils.setEmbankments(railways, embankments, 'railway', exclude_embankment_types=man_made_embankment_types, logger=logger)

# get remaining embankments
embankments = embankments.loc[(embankments['highway'].isnull()) & (embankments['railway'].isnull()) & (embankments['waterway'].isnull())]# | (embankments['overrule_infra']==1)]
logger.info('Embankments separate of infrastructure features: ' + str(len(embankments)))
if len(embankments) > 0:
    separate_embankments = True
else:
    separate_embankments = False

# add hierarchy for roads and sort on this
road_hierarchy = pd.read_csv(os.path.join(dir_filters, 'road_hierarchy.csv'))
roads.insert(len(roads.columns), 'road_hierarchy', list(np.full(len(roads.index), road_hierarchy['value'].max()+1)))
for road_type in road_hierarchy['highway'].values:
    roads.loc[roads['highway']==road_type, 'road_hierarchy'] = int(road_hierarchy.loc[road_hierarchy['highway']==road_type, 'value'].values[0])
roads.sort_values(by='road_hierarchy', ascending=False, inplace=True)

# adjust layer property for roads (doing this on layer_height would be more accurate, but adjusting layer property prevents doing this again for ramps later on)
logger.info('Setting layer property to zero for roads crossing culverts...')
roads.loc[(roads['layer']>0) & (roads.intersects(tunnels.loc[tunnels['tunnel']=='culvert'].unary_union)), 'layer'] = 0

logger.info('Setting layer property to zero for user-specified roads that should not be elevated...')
roads.loc[(roads['layer']>0) & (roads['highway'].isin(not_elevated_road_types)), 'layer'] = 0
roads.loc[(roads['layer']>0) & (roads['surface'].isin(not_elevated_road_surfaces)), 'layer'] = 0

# get sidewalks from roads
if do_sidewalks:
    logger.info('Extracting sidewalks from roads...')
    sidewalks = roads[roads.drive_frac<1].copy()
    if sidewalks.empty:
        logger.warning('No sidewalks found! Skipping all further sidewalk processing steps!')
        do_sidewalks = False
    else:
        # split up MultiLineString entries (buffering of sidewalks only works with regular LineStrings)
        logger.info('Splitting up MultiLineString sidewalks into separate entries...')
        sidewalks = utils.splitMultiGeoms(sidewalks)

# figures of grouped features (after application of filters)
if do_figures:
    # prep figure input
    dfs_all         = [roads, railways, waterways, buildings, water_polys, aeroways]
    base_titles_all = ['roads', 'railways', 'waterways', 'buildings', 'water', 'aeroways']
    col_select_all  = ['highway', 'railway', 'waterway', 'building', 'water', 'aeroway']
    titles_type_all = ['road types', 'railway types', 'waterway types', 'building types', 'water types', 'aeroway types']
    if do_sidewalks:
        dfs_all.insert(1, sidewalks)
        base_titles_all.insert(1, 'sidewalks')
        col_select_all.insert(1, 'highway')
        titles_type_all.insert(1, 'sidewalk road types')
    dfs_figure  = []
    dfs_select  = []
    base_titles = []
    col_select  = []
    titles_type = []
    for i in range(len(dfs_all)):
        if not dfs_all[i].empty:
            dfs_figure.append(dfs_all[i])
            base_titles.append(base_titles_all[i])
            col_select.append(col_select_all[i])
            titles_type.append(titles_type_all[i])
            dfs_select.append(dfs_all[i][col_select_all[i]])
    # create figures
    create_figures.subPlots(rows=3, cols=2, dfs=dfs_figure, base_titles=base_titles, path=path_output, filename='ParTerra_OSM_features_filtered')
    create_figures.histogramsForColumn(rows=3, cols=2, dfs=dfs_select, titles=titles_type, path=path_output, filename='ParTerra_OSM_types_by_features_filtered')

# buffer line segments based on width
logger.info('Buffering line segments based on widths...')
utils.bufferAll(waterways, 'waterways', do_figures, path_figures=path_output)
utils.bufferAll(railways, 'railways', do_figures, path_figures=path_output)
if separate_embankments:
    utils.bufferAll(embankments, 'embankments', do_figures, path_figures=path_output)
if do_sidewalks:
    if do_straightening:
        roads_for_straightening = roads.copy()
        utils.bufferAll(roads_for_straightening, 'roads', do_figures=False, do_sidewalks=False)
    utils.bufferAll(roads, 'roads', do_figures, do_sidewalks, path_figures=path_output)
    utils.bufferAll(sidewalks, 'sidewalks', do_figures, do_sidewalks, path_figures=path_output)
    logger.info('Preventing sidewalks from blocking roads/railways...')
    # remove sidewalks with overlaps of roads and railways on the same layer
    for layer in sidewalks.layer.unique():
        if roads.loc[roads.layer==layer].index.size > 0:
            sidewalks.loc[sidewalks.layer==layer] = gpd.overlay(sidewalks.loc[sidewalks.layer==layer], roads.loc[roads.layer==layer], 'difference')
        if railways.loc[railways.layer==layer].index.size > 0:
            sidewalks.loc[sidewalks.layer==layer] = gpd.overlay(sidewalks.loc[sidewalks.layer==layer], railways.loc[railways.layer==layer], 'difference')
    sidewalks = sidewalks.loc[sidewalks.geometry.notnull()] # remove any rows/features which had their geometries completely removed due to complete overlap
    if do_figures:
        create_figures.singlePlot(sidewalks, show_count=False, base_title='ParTerra sidewalks (polygons)', path=path_output, filename='ParTerra_sidewalks_buffered_road_rail_overlaps_removed')
else:
    utils.bufferAll(roads, 'roads', do_figures, do_sidewalks, path_figures=path_output)
    if do_straightening:
        roads_for_straightening = roads.copy()

# clip waterways geometries on water polygons
logger.info('Cutting overlaps of buffered waterways with water polygons...')
waterways = gpd.overlay(waterways, water_polys, 'difference')

# calculate DTM/DSM heights
logger.info('Calculating DTM/DSM heights...')
utils.calcHeights(buildings, ['height'], column_layer=building_levels_key, column_layer_height='level_height')
utils.calcHeights(buildings, ['DTM_height'], column_offset=building_DTM_label)
utils.calcHeights(buildings, ['DSM_height'], column_offset='height')
utils.calcHeights(waterways, ['DTM_height', 'DSM_height'], column_offset='depth', factor=-1)
utils.calcHeights(water_polys, ['DTM_height', 'DSM_height'], column_offset='depth', factor=-1)
utils.calcHeights(railways, ['DTM_height', 'DSM_height'], column_offset='offset', column_layer='layer', column_layer_height='layer_height', column_embank='embank_height')
if separate_embankments:
    utils.calcHeights(embankments, ['DTM_height', 'DSM_height'], column_offset='height')
if do_sidewalks:
    utils.calcHeights(roads, ['DTM_height', 'DSM_height'], column_offset='driveway_offset', column_layer='layer', column_layer_height='layer_height', column_embank='embank_height')
    utils.calcHeights(sidewalks, ['DTM_height', 'DSM_height'], column_offset='sidewalk_offset', column_layer='layer', column_layer_height='layer_height', column_embank='embank_height')
else:
    utils.calcHeights(roads, ['DTM_height', 'DSM_height'], column_offset='driveway_offset', column_layer='layer', column_layer_height='layer_height', column_embank='embank_height', column_subtract='sidewalk_offset')

# writing to shapefiles
if do_shapefiles:
    logger.info('Writing processed OSM data to shapefiles...')
    buildings.to_file(os.path.join(path_output, dir_shapes_out, 'buildings.shp'))
    waterways.to_file(os.path.join(path_output, dir_shapes_out, 'waterways.shp'))
    water_polys.to_file(os.path.join(path_output, dir_shapes_out, 'water_polys.shp'))
    railways.to_file(os.path.join(path_output, dir_shapes_out, 'railways.shp'))
    roads.to_file(os.path.join(path_output, dir_shapes_out, 'roads.shp'))
    if separate_embankments:
        embankments.to_file(os.path.join(path_output, dir_shapes_out, 'embankments.shp'))
    if do_sidewalks:
        sidewalks.to_file(os.path.join(path_output, dir_shapes_out, 'sidewalks.shp'))
        if do_straightening:
            roads_for_straightening.to_file(os.path.join(path_output, dir_shapes_out, 'roads_for_straightening.shp'))

# construct dictionary with all OSM features
OSM_features = {
    'buildings': {
        'data': buildings,
        'separate': False,
        'label_DTM': 'DTM_height',
        'label_DSM': 'DSM_height',
        'all_touched': False,
        'label_sorting': None,
        'sort_ascending': True
    },
    'waterways': {
        'data': waterways,
        'separate': False,
        'label_DTM': 'DTM_height',
        'label_DSM': 'DSM_height',
        'all_touched': all_touched,
        'label_sorting': 'depth',
        'sort_ascending': True
    },
    'water_polys': {
        'data': water_polys,
        'separate': False,
        'label_DTM': 'DTM_height',
        'label_DSM': 'DSM_height',
        'all_touched': all_touched,
        'label_sorting': 'depth',
        'sort_ascending': True
    },
    'railways': {
        'data': railways,
        'separate': True,
        'label_DTM': 'DTM_height',
        'label_DSM': 'DSM_height',
        'all_touched': all_touched,
        'label_sorting': None,
        'sort_ascending': True
    }
}

# add embankments
if separate_embankments:
    OSM_features['embankments'] = {
        'data': embankments,
        'separate': False,
        'label_DTM': 'DTM_height',
        'label_DSM': 'DSM_height',
        'all_touched': all_touched,
        'label_sorting': None,
        'sort_ascending': True
    }
else:
    OSM_features['embankments'] = None

# add roads and sidewalks
if do_sidewalks:
    OSM_features['roads'] = {
        'data': roads,
        'separate': True,
        'label_DTM': 'DTM_height',
        'label_DSM': 'DSM_height',
        'all_touched': all_touched,
        'label_sorting': None,
        'sort_ascending': True
    }
    OSM_features['sidewalks'] = {
        'data': sidewalks,
        'separate': True,
        'label_DTM': 'DTM_height',
        'label_DSM': 'DSM_height',
        'all_touched': all_touched,
        'label_sorting': None,
        'sort_ascending': True
    }
else:
    OSM_features['roads'] = {
        'data': roads,
        'separate': True,
        'label_DTM': 'DTM_height',
        'label_DSM': 'DSM_height',
        'all_touched': all_touched,
        'label_sorting': None,
        'sort_ascending': True
    }
    OSM_features['sidewalks'] = None

# add natural
if do_manning:
    OSM_features['natural'] = {
        'data': natural,
        'separate': False,
        'label_DTM': 'height',
        'label_DSM': 'height',
        'all_touched': all_touched,
        'label_sorting': None,
        'sort_ascending': True
    }
    OSM_features['landuse'] = {
        'data': landuse,
        'separate': False,
        'label_DTM': 'height',
        'label_DSM': 'height',
        'all_touched': all_touched,
        'label_sorting': None,
        'sort_ascending': True
    }
    OSM_features['aeroways'] = {
        'data': aeroways,
        'separate': False,
        'label_DTM': 'height',
        'label_DSM': 'height',
        'all_touched': all_touched,
        'label_sorting': None,
        'sort_ascending': True
    }

# create folder for storing all rasters/grids
if not os.path.exists(os.path.join(path_output, dir_rasters_out)):
    os.mkdir(os.path.join(path_output, dir_rasters_out))

# DEM straightening
if do_straightening:
    
    # separate into features for DTM/DSM
    roads_for_straightening_for_DTM = roads_for_straightening.loc[roads_for_straightening['layer']<=0].copy()
    roads_for_straightening_for_DSM = roads_for_straightening.copy()
    
    # set up temp dir
    temp_dir = utils.setupTempDir(os.path.join(path_output, dir_rasters_out), 'temp')
    
    # straighten DEM along single feature (part)
    def straightenDEM_parts(part):
        # obtain base DEM part covered by current segment
        shapes = [part['geometry'].__geo_interface__]
        # https://rasterio.readthedocs.io/en/latest/api/rasterio.mask.html
        DEM_part = rasterio.mask.mask(src, shapes=shapes, all_touched=all_touched, nodata=-999, crop=True)
        DEM_part[0][0][np.where(DEM_part[0][0]==-999)] = np.nan
        # function used with scipy.ndimage.filters.generic_filter for straightening
        def straightening_func(a):
            # https://docs.scipy.org/doc/numpy/reference/generated/numpy.nanmean.html
            return np.nanmean(a)
        # obtain window/kernel size (number of cells/pixels)
        size = np.int(np.ceil((part['width']*4)/resolution))
        # perform straightening
        if size > 1:
            # https://docs.scipy.org/doc/scipy/reference/generated/scipy.ndimage.generic_filter.html
            DEM_straightened = scipy.ndimage.filters.generic_filter(input=DEM_part[0][0],function=straightening_func,size=size,mode='constant',cval=np.nan)
        else:
            DEM_straightened = DEM_part[0][0]
        # write to temporary file
        DEM_straightened[np.where(np.isnan(DEM_straightened))] = -999
        map_meta_temp = map_meta.copy()
        map_meta_temp['width'] = DEM_straightened.shape[1]
        map_meta_temp['height'] = DEM_straightened.shape[0]
        map_meta_temp['transform'] = DEM_part[1]
        map_meta_temp['nodata'] = -999
        temp_path = os.path.join(temp_dir, str(part['osm_id'])+'.tif')
        if os.path.exists(temp_path):
            os.remove(temp_path)
        utils.writeRasterToFile(DEM_straightened, temp_path, map_meta_temp)
        # overwrite temporary file with clip to current segment (scipy.ndimage.filters.generic_filter can create values outside of this geometry)
        src_temp = rasterio.open(temp_path, 'r')
        DEM_straightened_clip = rasterio.mask.mask(src_temp, shapes=shapes, all_touched=all_touched, nodata=-999, crop=True)
        src_temp.close()
        os.remove(temp_path)
        utils.writeRasterToFile(DEM_straightened_clip[0][0], temp_path, map_meta_temp)
    
    # full pipeline for DEM straightening
    def straightenDEM(base_DEM, DEM_type, features, sort_column, sort_asc=False, dissolve_column='width', agg_func='min', filename_dissolved=None):
        # clean up temp dir
        utils.setupTempDir(os.path.join(path_output, dir_rasters_out), 'temp')
        # dissolve features
        if dissolve_column != None:
            relevant_columns = ['osm_id','width','geometry',sort_column]
            features_for_straightening = utils.dissolveFeatures(features.loc[:,relevant_columns], dissolve_column, agg_func)
            if do_shapefiles and filename_dissolved != None:
                features_for_straightening.to_file(os.path.join(path_output, dir_shapes_out, filename_dissolved + '.shp'))
        else:
            features_for_straightening = features.copy()
        # sort features
        features_for_straightening.sort_values(by=sort_column, ascending=sort_asc, inplace=True)
        # create straightened DEM parts for each feature
        features_for_straightening.apply(lambda row: straightenDEM_parts(row), axis=1)
        # merge straightened parts with base DEM
        # https://rasterio.readthedocs.io/en/latest/api/rasterio.merge.html
        to_merge_files = [r for r in os.listdir(temp_dir) if 'xml' not in r]
        if len(to_merge_files) == len(features_for_straightening):
            to_merge_files = reversed([r + '.tif' for r in features_for_straightening['osm_id'].values])
        else:
            logger.error('Number of individually straightened files does not match the number of features in dataframe! This can result in errors in straightened DEM!')
        to_merge_datasets = [rasterio.open(os.path.join(temp_dir, r), 'r') for r in to_merge_files]
        to_merge_datasets.append(src)
        DEM_straightened = rasterio.merge.merge(to_merge_datasets, res=resolution, nodata=map_meta['nodata'])
        DEM_straightened = DEM_straightened[0][0]
        DEM_straightened[np.where(np.isnan(base_DEM))] = np.nan
        utils.writeRasterToFile(DEM_straightened, os.path.join(path_output, dir_rasters_out, 'DEM_straightened_for_' + DEM_type + '.tif'), map_meta, digits)
        return DEM_straightened
    
    # straighten DEM for specific features
    logger.info('Straightening base DEM along roads...')
    DEM_straightened_for_DTM = straightenDEM(base_DEM, 'DTM', roads_for_straightening_for_DTM, sort_column='road_hierarchy', dissolve_column='width', agg_func='min', filename_dissolved='roads_DTM_dissolved_width')
    DEM_straightened_for_DSM = straightenDEM(base_DEM, 'DSM', roads_for_straightening_for_DSM, sort_column='road_hierarchy', dissolve_column='width', agg_func='min', filename_dissolved='roads_DSM_dissolved_width')
    src.close()
    if do_tiles:
        src_1 = rasterio.open(os.path.join(path_output, dir_rasters_out, 'DEM_straightened_for_DTM.tif'))
        src_2 = rasterio.open(os.path.join(path_output, dir_rasters_out, 'DEM_straightened_for_DSM.tif'))

# rasterizing and DEM creation
if do_tiles:
    logger.info('Performing rasterizing and further processing in tiles...')
    # process each tile
    for x_offset in range(0, tiles_x):
        for y_offset in range(0, tiles_y):
            logger.info('Processing tile ' + str(x_offset) + ',' + str(y_offset) + '...')
            # get tile and related info
            df_tile = df_tiles.loc[(df_tiles['tile_x']==int(x_offset)) & (df_tiles['tile_y']==int(y_offset))]
            tile_meta['transform'] = df_tile['tile_transform'].values[0]
            tile_mask = [df_tile.geometry.values[0].__geo_interface__]
            # clip base DEM and set tile shape
            if do_straightening or use_previously_straightened:
                clip_DEM_straightened_for_DTM, clip_transform = rasterio.mask.mask(src_1, shapes=tile_mask, all_touched=all_touched, nodata=np.nan, crop=True)
                clip_DEM_straightened_for_DSM, clip_transform = rasterio.mask.mask(src_2, shapes=tile_mask, all_touched=all_touched, nodata=np.nan, crop=True)
                tile_shape = clip_DEM_straightened_for_DSM[0].shape
            else:
                clip_base_DEM, clip_transform = rasterio.mask.mask(src, shapes=tile_mask, all_touched=all_touched, nodata=np.nan, crop=True)
                tile_shape = clip_base_DEM[0].shape
            if clip_transform != tile_meta['transform']:
                logger.warning('  affine transform of tile and clipped base DEM are not the same!')
            # convert OSM features to raster/grid
            logger.info('  rasterizing OSM features...')
            rasterized_features = utils.convertAllToRaster(tile_shape, tile_meta, OSM_features)
            if do_manning:
                # drop features that are only used for Manning's roughness
                del rasterized_features['DTM']['natural']
                del rasterized_features['DSM']['natural']
                del rasterized_features['DTM']['landuse']
                del rasterized_features['DSM']['landuse']
                del rasterized_features['DTM']['aeroways']
                del rasterized_features['DSM']['aeroways']
            # create GeoTIFF files for each rasterized OSM feature
            if do_feature_maps:
                logger.info('  writing rasterized OSM features to GeoTIFFs...')
                utils.writeAllRastersToFile(rasterized_features, os.path.join(path_output, dir_rasters_out, 'tiles'), tile_meta, digits, '_' + str(x_offset) + '_' + str(y_offset))
            # create relative elevation maps
            logger.info('  calculating relative elevation maps...')
            rel_DTM = utils.calcRelativeDEM(rasterized_features, 'DTM')
            rel_DSM = utils.calcRelativeDEM(rasterized_features, 'DSM')
            if do_relative_maps:
                logger.info('  writing relative elevation maps to GeoTIFFs...')
                utils.writeRasterToFile(rel_DTM, os.path.join(path_output, dir_rasters_out, 'tiles', 'relative_DTM_' + str(x_offset) + '_' + str(y_offset) + '.tif'), tile_meta, digits=digits)
                utils.writeRasterToFile(rel_DSM, os.path.join(path_output, dir_rasters_out, 'tiles', 'relative_DSM_' + str(x_offset) + '_' + str(y_offset) + '.tif'), tile_meta, digits=digits)
            # create absolute elevation maps
            logger.info('  calculating absolute elevation maps...')
            if do_straightening or use_previously_straightened:
                DTM = clip_DEM_straightened_for_DTM[0] + rel_DTM
                DSM = clip_DEM_straightened_for_DSM[0] + rel_DSM
            else:
                DTM = clip_base_DEM[0] + rel_DTM
                DSM = clip_base_DEM[0] + rel_DSM
            logger.info('  writing absolute elevation maps to GeoTIFFs...')
            utils.writeRasterToFile(DTM, os.path.join(path_output, dir_rasters_out, 'tiles', 'DTM', 'DTM_' + str(x_offset) + '_' + str(y_offset) + '.tif'), tile_meta, digits=digits)
            utils.writeRasterToFile(DSM, os.path.join(path_output, dir_rasters_out, 'tiles', 'DSM', 'DSM_' + str(x_offset) + '_' + str(y_offset) + '.tif'), tile_meta, digits=digits)
            # create Manning's roughness map
            if do_manning:
                logger.info("  calculating Manning's roughness map...")
                manning = utils.calcRelativeDEM(rasterized_features, 'Manning', base_val=0.02)
                manning = np.where(np.isnan(DTM), np.nan, manning)
                logger.info("  writing Manning's roughness map to GeoTIFF...")
                utils.writeRasterToFile(manning, os.path.join(path_output, dir_rasters_out, 'tiles', 'Manning', 'Manning_roughness_' + str(x_offset) + '_' + str(y_offset) + '.tif'), tile_meta, digits=4)
    # close datasets
    if do_straightening or use_previously_straightened:
        src_1.close()
        src_2.close()
    else:
        src.close()
    # merge tiles
    #utils.mergeTilesRasterio(os.path.join(path_output, dir_rasters_out), 'DTM', map_meta, resolution, digits, logger=logger)
    #utils.mergeTilesRasterio(os.path.join(path_output, dir_rasters_out), 'DSM', map_meta, resolution, digits, logger=logger)
    utils.mergeTilesGDAL(os.path.join(path_output, dir_rasters_out), 'DTM', dir_gdal_merge, logger=logger)
    utils.mergeTilesGDAL(os.path.join(path_output, dir_rasters_out), 'DSM', dir_gdal_merge, logger=logger)
    if do_manning:
        #utils.mergeTilesRasterio(os.path.join(path_output, dir_rasters_out), 'Manning', map_meta, resolution, digits=4, logger=logger)
        utils.mergeTilesGDAL(os.path.join(path_output, dir_rasters_out), 'Manning', dir_gdal_merge, logger=logger)
else:
    # convert OSM features to raster/grid
    logger.info('Rasterizing OSM features...')
    rasterized_features = utils.convertAllToRaster(base_shape, map_meta, OSM_features)
    if do_manning:
        # drop features that are only used for Manning's roughness
        del rasterized_features['DTM']['natural']
        del rasterized_features['DSM']['natural']
        del rasterized_features['DTM']['landuse']
        del rasterized_features['DSM']['landuse']
        del rasterized_features['DTM']['aeroways']
        del rasterized_features['DSM']['aeroways']

    # create GeoTIFF files for each rasterized OSM feature
    if do_feature_maps:
        logger.info('Writing rasterized OSM features to GeoTIFFs...')
        utils.writeAllRastersToFile(rasterized_features, os.path.join(path_output, dir_rasters_out), map_meta, digits)

    # create relative elevation maps
    logger.info('Calculating relative elevation maps...')
    rel_DTM = utils.calcRelativeDEM(rasterized_features, 'DTM')
    rel_DSM = utils.calcRelativeDEM(rasterized_features, 'DSM')
    if do_relative_maps:
        logger.info('Writing relative elevation maps to GeoTIFFs...')
        utils.writeRasterToFile(rel_DTM, os.path.join(path_output, dir_rasters_out, 'relative_DTM.tif'), map_meta, digits=digits)
        utils.writeRasterToFile(rel_DSM, os.path.join(path_output, dir_rasters_out, 'relative_DSM.tif'), map_meta, digits=digits)

    # create absolute elevation map
    logger.info('Calculating absolute elevation maps...')
    if do_straightening or use_previously_straightened:
        DTM = DEM_straightened_for_DTM + rel_DTM
        DSM = DEM_straightened_for_DSM + rel_DSM
    else:
        DTM = base_DEM + rel_DTM
        DSM = base_DEM + rel_DSM
    logger.info('Writing absolute elevation maps to GeoTIFFs...')
    utils.writeRasterToFile(DTM, os.path.join(path_output, dir_rasters_out, 'DTM.tif'), map_meta, digits=digits)
    utils.writeRasterToFile(DSM, os.path.join(path_output, dir_rasters_out, 'DSM.tif'), map_meta, digits=digits)
    
    # create Manning's roughness map
    if do_manning:
        logger.info("Calculating Manning's roughness map...")
        manning = utils.calcRelativeDEM(rasterized_features, 'Manning', base_val=0.02)
        manning = np.where(np.isnan(DTM), np.nan, manning)
        logger.info("Writing Manning's roughness map to GeoTIFF...")
        utils.writeRasterToFile(manning, os.path.join(path_output, dir_rasters_out, 'Manning_roughness.tif'), map_meta, digits=4)

# close logger
handle_logging.getStats(logger)
logger.info('Finished ParTerra processing.')
handle_logging.closeLogging()
