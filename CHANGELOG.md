# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

### Added
- allow command line call to obtain UTM zone

### Fixed
- Manning's roughness with tiled output
- general no data handling with tiled output
- waterway LinearRing conversion (when none present)
- removal of irrelevant columns (when none present)
- rasterizing OSM features (when none present)
- relative DTM/DSM calculation (when no railways present)

## [1.1.0] - 2020-12-17

### Added
- Manning's roughness map
- QGIS template and script for updating
- automatically derived UTM zone from study area
- conversion of waterway LinearRings to polygons
- automatic removal of waterway features that obstruct flow
- processing of aeroway features
- option to set label for building DTM height (default 'threshold', as it was)

### Changed
- removed irrelevant columns from all features
- improved code for calculation of heights

## [1.0.0] - 2020-08-25

### Added
- embankments (for roads and railways, as well as separate, such as levees)
- option to perform processing in tiles (for very large areas)
- first version of DEM preprocessing (resampling + smoothing)
- Google Earth Engine DEM prepartion script in legacy folder
- more default OSM fixes for values that should be numerical entries

### Changed
- updated documentation
- large overhaul of main script, moved a lot to functions to utils
- updated OSM export YAML file

### Removed
- removed tests.py (old/tests/development/unfinished code)

### Fixed
- select only relevant (numeric) dataframe columns when doing DEM straightening
- only use non-empty dataframes as input for figures and shapefiles
- fixed cleanup up of temp dir for DEM straightening

## [0.2.0] - 2020-07-05

### Added
- DEM straightening along roads
- road hierarchy (importance score) filter, used for DEM straightening
- option to use previously straightened DEMs instead of raw base DEM
- use of SciPy package (for DEM straightening)
- users can now specify OSM attributes to be corrected (replacing an invalid numeric entry with a valid one)
- automatic tunnel removal
- support for BigTIFF

### Changed
- default value of 'all_touched' parameter set to False (i.e. only pixels that are completely covered by a vector feature are used)
- corrections on OSM attributes are no longer completely hard-coded, only those often occuring are kept within the code
- conda environment using more recent packages (also required for BigTIFF support)

### Fixed
- fix for MultiLineString sidewalks (relevant when roads are split into multiple sections due to project area)
- fix for log / command line warnings related to OSM id's when writing shapefiles
- fixed road sorting order when converting to raster

## [0.1.0] - 2020-04-14
First development version
